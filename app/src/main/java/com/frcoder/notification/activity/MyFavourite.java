package com.frcoder.notification.activity;

import com.frcoder.notification.ModelsClasses.MyFavouriteListMain;
import com.frcoder.notification.ModelsClasses.MyFavouriteListMainData;
import com.frcoder.notification.R;
import com.frcoder.notification.RetrofitApiCalling.RetrofitHelper;
import com.frcoder.notification.adapters.FavouriteListAdapter;
import com.frcoder.notification.constans.Constant;
import com.frcoder.notification.constans.NetworkUtil;
import com.frcoder.notification.constans.SharedPreferencesData;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFavourite extends AppCompatActivity {

    @BindView(R.id.imgView_navigationIcon1)
    ImageView imgView_navigationIcon1;


    @BindView(R.id.recyclerView_favourite)
    RecyclerView recyclerView_favourite;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private SharedPreferencesData sharedPreferencesData;
    private String userId;
    private List<MyFavouriteListMainData> myFavouriteListMainData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_my_favourite);
        ButterKnife.bind(this);
        viewFinds();
    }
    //use to intialize all view of this class
    private void viewFinds() {
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        userId=sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE,Constant.ID);
        recyclerView_favourite.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, true);
        layoutManager.setReverseLayout(false);
        recyclerView_favourite.setLayoutManager(layoutManager);




        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().setFavouriteList(setFavouriteList,userId);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().setFavouriteList(setFavouriteList,userId);
        }
    }

    Callback<MyFavouriteListMain> setFavouriteList = new Callback<MyFavouriteListMain>() {
        @Override
        public void onResponse(Call<MyFavouriteListMain> call, Response<MyFavouriteListMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess()==1){
                    if (response.body()!=null){
                        myFavouriteListMainData=new ArrayList<>();
                        myFavouriteListMainData.clear();
                        myFavouriteListMainData.addAll(response.body().getNotificationList());
                        FavouriteListAdapter favouriteListAdapter=new FavouriteListAdapter(getApplicationContext(),myFavouriteListMainData);
                        recyclerView_favourite.setAdapter(favouriteListAdapter);
                    }

                }else{
                    Snackbar.make(recyclerView_favourite,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }

        }

        @Override
        public void onFailure(Call<MyFavouriteListMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    //back imageView click listner for backpressed
    @OnClick(R.id.imgView_navigationIcon1)
    public void backClick(View view) {
        onBackPressed();
    }
}
