package com.frcoder.notification.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.frcoder.notification.ModelsClasses.SocialLinkMain;
import com.frcoder.notification.ModelsClasses.SocialLinkMainData;
import com.frcoder.notification.R;
import com.frcoder.notification.RetrofitApiCalling.RetrofitHelper;
import com.frcoder.notification.constans.NetworkUtil;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class SocialLinks extends AppCompatActivity {
    @BindView(R.id.recyclerView_socialLinks)
    RecyclerView recyclerView_socialLinks;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    MySocialLinkAdapter myNotificationAdapter;
    private List<SocialLinkMainData> socialLinkMains;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_links);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        recyclerView_socialLinks.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, true);
        layoutManager.setReverseLayout(false);
        recyclerView_socialLinks.setLayoutManager(layoutManager);


        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().setSocialList(socialLinkCallback);
        }

    }

    Callback<SocialLinkMain> socialLinkCallback = new Callback<SocialLinkMain>() {
        @Override
        public void onResponse(Call<SocialLinkMain> call, Response<SocialLinkMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess()){
                    if (response.body()!=null){
                        socialLinkMains=new ArrayList<>();
                        socialLinkMains.clear();
                        socialLinkMains.addAll(response.body().getResult());
                        myNotificationAdapter = new MySocialLinkAdapter();
                        recyclerView_socialLinks.setAdapter(myNotificationAdapter);
                    }

                }else{
                    Snackbar.make(recyclerView_socialLinks,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }

        }

        @Override
        public void onFailure(Call<SocialLinkMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    public class MySocialLinkAdapter extends RecyclerView.Adapter<MySocialLinkAdapter.ViewHolder> {


        public MySocialLinkAdapter() {

        }

        @Override
        public MySocialLinkAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.items_my_notification, parent, false);
            MySocialLinkAdapter.ViewHolder viewHolder = new MySocialLinkAdapter.ViewHolder(listItem);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(MySocialLinkAdapter.ViewHolder holder, final int position) {
            //String date = serverDateToLocalDate(unreadNotificationDataList.get(position).getDate());
            holder.tv_offerType.setText(socialLinkMains.get(position).getTitle());
            holder.layoutLinear_offer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+socialLinkMains.get(position).getSocialUrl()));
                    startActivity(browserIntent);
                }
            });


        }


        @Override
        public int getItemCount() {
            return socialLinkMains.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView tv_tips_text, tv_date, tv_offerType;
            private LinearLayout layoutLinear_offer;

            public ViewHolder(View itemView) {
                super(itemView);
                tv_offerType=itemView.findViewById(R.id.tv_offerType);
                layoutLinear_offer=itemView.findViewById(R.id.layoutLinear_offer);



            }
        }
    }





    @OnClick(R.id.imgView_navigationIcon1)
    public void onBackClicked(View view){
        onBackPressed();
    }



}
