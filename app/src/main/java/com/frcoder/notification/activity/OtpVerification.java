package com.frcoder.notification.activity;

import android.content.Intent;

import com.frcoder.notification.ModelsClasses.UserMain;
import com.frcoder.notification.R;
import com.frcoder.notification.RetrofitApiCalling.RetrofitHelper;
import com.frcoder.notification.constans.Constant;
import com.frcoder.notification.constans.NetworkUtil;
import com.frcoder.notification.constans.SharedPreferencesData;
import com.google.android.gms.tasks.OnSuccessListener;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpVerification extends AppCompatActivity {
    @BindView(R.id.pinview)
    Pinview pinview;

    @BindView(R.id.btn_verify)
    Button btn_verify;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private boolean otpValueFilled = false;
    private String otpData = "";
    private String otpDataFromPinView = "";
    private String mobileNumber = "";
    private String completeStatus = "";
    private String newToken = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        setContentView(R.layout.activity_otp_verification);
        ButterKnife.bind(this);
        viewFinds();

    }

    private void viewFinds() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(OtpVerification.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken= instanceIdResult.getToken();
                Log.e("newToken", newToken);

            }
        });
        //completeStatus = getIntent().getStringExtra(Constant.COMPLETE_STATUS);
        otpData = getIntent().getStringExtra(Constant.OTP);
        mobileNumber = getIntent().getStringExtra(Constant.MOBILE_NUMBER);
        otpData = getIntent().getStringExtra(Constant.OTP);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                if (pinview.getValue().length() < 4) {
                    otpValueFilled = false;
                } else {
                    otpValueFilled = true;
                }
                Log.e("pinLeft", pinview.getValue() + "  " + fromUser);
                otpDataFromPinView = pinview.getValue();

            }
        });
        btn_verify.setOnClickListener(btn_verifyListner);
    }

    private View.OnClickListener btn_verifyListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (otpValueFilled) {

               /* if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                    progressBar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().setUserCheck(getUserData, mobileNumber);

                }*/


                if (otpData != null) {
                    if (otpData.equalsIgnoreCase("")) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_again_try), Toast.LENGTH_LONG).show();
                    } else if (otpData.equalsIgnoreCase(otpDataFromPinView)) {
                        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                            progressBar.setVisibility(View.VISIBLE);
                            RetrofitHelper.getInstance().setUserCheck(getUserData, mobileNumber,newToken);
                            //RetrofitHelper.getInstance().setUserCreate(getUserData, mobileNumber, "", "", "", "");

                        }
                       /* Toast.makeText(getApplicationContext(), getResources().getString(R.string.otp_verified), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), GenderActivity.class);
                        intent.putExtra(Constant.MOBILE_NUMBER, mobileNumber);
                        startActivity(intent);*/
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_valid_otp), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_again_try), Toast.LENGTH_LONG).show();
                }
                /*}else{
                    Snackbar.make(getCurrentFocus(), getResources().getString(R.string.enter_valid_otp), Snackbar.LENGTH_LONG).show();*/

               /* if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                    progressBar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().setUserCheck(getUserData, mobileNumber);
                    //RetrofitHelper.getInstance().setUserCreate(getUserData, mobileNumber, "", "", "", "");

                }*/
            }else{
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_otp), Toast.LENGTH_LONG).show();
            }




        }
    };

    Callback<UserMain> getUserData = new Callback<UserMain>() {
        @Override
        public void onResponse(Call<UserMain> call, Response<UserMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess() == 1) {
                    SharedPreferencesData sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
                    sharedPreferencesData.setSharedPreferenceData(Constant.ID, Constant.MOBILE_NUMBER, response.body().getUserDetails().getId());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.MOBILE_NUMBER, response.body().getUserDetails().getNumber());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.GENDER, response.body().getUserDetails().getGender());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.FIRST_NAME, response.body().getUserDetails().getFirstname());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.LAST_NAME, response.body().getUserDetails().getLastname());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.CATEGORY, response.body().getUserDetails().getCategory());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.USERIMAGE, response.body().getUserDetails().getImage());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.DATE_OF_BIRTH, response.body().getUserDetails().getDob());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.ADDRESS, response.body().getUserDetails().getAddress());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.EMAIL, response.body().getUserDetails().getEmail());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.STATUS, response.body().getUserDetails().getStatus());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.COMPLETE_STATUS, response.body().getUserDetails().getCompleteStatus());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.ADDEDAT, response.body().getUserDetails().getAddedAt());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.ID, response.body().getUserDetails().getId());
                    Log.v("user_id=",response.body().getUserDetails().getId());
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.otp_verified), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), GenderActivity.class);
                    intent.putExtra(Constant.MOBILE_NUMBER, mobileNumber);
                    startActivity(intent);
                }


            }

        }

        @Override
        public void onFailure(Call<UserMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

}
