package com.frcoder.notification.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Interpolator;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.frcoder.notification.ModelsClasses.UnreadCountMain;
import com.frcoder.notification.ModelsClasses.CategoryListMain;
import com.frcoder.notification.ModelsClasses.CategoryListMainData;
import com.frcoder.notification.ModelsClasses.SlideImageMain;
import com.frcoder.notification.ModelsClasses.SliderImageMainData;
import com.frcoder.notification.R;
import com.frcoder.notification.RetrofitApiCalling.RetrofitHelper;
import com.frcoder.notification.adapters.CategoryListAdapterMainActivity;
import com.frcoder.notification.constans.Constant;
import com.frcoder.notification.constans.NetworkUtil;
import com.frcoder.notification.constans.SharedPreferencesData;
import com.frcoder.notification.fragment.MyDialogFragment;
import com.frcoder.notification.fragment.TestFragment;
import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Scroller;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nikartm.support.BadgePosition;
import ru.nikartm.support.ImageBadgeView;

//created by Arif Ali
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.imgView_navigationIcon)
    ImageView imgView_navigationIcon;

    @BindView(R.id.layout_unreadNotification)
    LinearLayout layout_unreadNotification;

    @BindView(R.id.recyclerview_slides)
    RecyclerView recyclerview_slides;
    SlideTopAdapter slideTopAdapter;

    @BindView(R.id.viewpager)
    ViewPager viewpager;


    TextView tv_name;
    TextView tv_emailAddress;
    CircularImageView imageView;


    SharedPreferencesData sharedPreferencesData;
    private String image = "";
    private String firstName = "";
    private String lastName = "";
    private String emailAddress = "";
    NavigationView navigationView;
    DrawerLayout drawer;
    View headerView;
    List<CategoryListMainData> categoryListMainData;

    @BindView(R.id.recyclerview_categoryList)
    RecyclerView recyclerview_categoryList;


    @BindView(R.id.imgview_badge)
    ImageBadgeView imgview_badge;
    private CategoryListAdapterMainActivity categoryListAdapterMainActivity;
    private String userId;
    private int bannerSize = 0;
    private int countNotificaton=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final CoordinatorLayout content = (CoordinatorLayout) findViewById(R.id.content);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
       /* ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);*/

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_open) {
            private float scaleFactor = 20f;

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float slideX = drawerView.getWidth() * slideOffset;
                content.setTranslationX(slideX);
                content.setScaleX(1 - (slideOffset / scaleFactor));
                content.setScaleY(1 - (slideOffset / scaleFactor));

            }
        };

        drawer.setScrimColor(Color.TRANSPARENT);
        drawer.setDrawerElevation(0f);
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        viewFinds();

    }
    private void viewFinds() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(MainActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.e("newToken", newToken);

            }
        });
        imgview_badge.setBadgeValue(0)
                .setBadgeOvalAfterFirst(true)
                .setBadgeTextSize(13)
                .setMaxBadgeValue(999)
                .setBadgeTextColor(getResources().getColor(R.color.white))
                //.setBadgeTextFont(typeface)
                //.setBadgeBackground(getResources().getDrawable(R.drawable.notification_main_page))
                .setBadgePosition(BadgePosition.TOP_RIGHT)
                .setBadgeTextStyle(Typeface.NORMAL)
                .setShowCounter(true)
                .setBadgePadding(4);


        //All sharedPreferences data save in a single class  SharedPreferenceData
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        sharedPreferencesData.createNewSharedPreferences(Constant.USERLOGIN);
        sharedPreferencesData.setSharedPreferenceData(Constant.USERLOGIN, Constant.FLAG, "true");


        imgView_navigationIcon.setOnClickListener(imgView_navigationIconListner);
        userId = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.ID);
        //for gridview list in this page
        recyclerview_categoryList.setHasFixedSize(true);
        recyclerview_categoryList.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerview_categoryList.setNestedScrollingEnabled(false);


        recyclerview_slides.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, true);
        layoutManager.setReverseLayout(false);
        recyclerview_slides.setLayoutManager(layoutManager);
        slideTopAdapter = new SlideTopAdapter();
        recyclerview_slides.setAdapter(slideTopAdapter);

        recyclerview_slides.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastItem = layoutManager.findLastCompletelyVisibleItemPosition();
                if (lastItem == layoutManager.getItemCount() - 1) {
                    mHandler.removeCallbacks(SCROLLING_RUNNABLE);
                    Handler postHandler = new Handler();
                    postHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //recyclerView_tips.setAdapter(null);

                            mHandler.postDelayed(SCROLLING_RUNNABLE, 1500);

                        }
                    }, 2000);
                }
            }
        });
        mHandler.postDelayed(SCROLLING_RUNNABLE, 2000);
    }

    private void ṁNavigationNameEmailSet() {

        userId = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.ID);
        firstName = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.FIRST_NAME);
        lastName = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.LAST_NAME);
        emailAddress = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.EMAIL);
        image = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.USERIMAGE);
        //Log.e("TotalName=", firstName + lastName);
        imageView = headerView.findViewById(R.id.imageView);
        tv_name = headerView.findViewById(R.id.tv_name);
        tv_emailAddress = headerView.findViewById(R.id.tv_emailAddress);

        if (firstName.equalsIgnoreCase("") && lastName.equalsIgnoreCase("")) {
            //for testing value set
            tv_name.setText("");
        } else {
            tv_name.setText(firstName + " " + lastName);
        }

        if (emailAddress.equalsIgnoreCase("")) {
            //for testing value set
            tv_emailAddress.setText("");
        } else {
            tv_emailAddress.setText(emailAddress);
        }

        if (image != null && (!image.equalsIgnoreCase(""))) {
            Picasso.with(this)
                    .load(image)
                    .placeholder(R.drawable.user)
                    .error(R.drawable.user)
                    .into(imageView);
        }
    }


    //This method is used to add viewpager accordingly images coming from server and
    //Showing them on Slider on MainActivity
    private void addTabs(int totalLength, ArrayList<SliderImageMainData> slideImageData) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (int i = 0; i < totalLength; i++) {
            adapter.addFrag(new TestFragment().newInstance(slideImageData.get(i).getTarget_url(), slideImageData.get(i).getLogo() + ""), "Test");
        }
        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            FixedSpeedScroller scroller = new FixedSpeedScroller(viewpager.getContext());
            // scroller.setFixedDuration(5000);
            mScroller.set(viewpager, scroller);
        } catch (NoSuchFieldException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        }
        viewpager.setClipToPadding(false);
        // set padding manually, the more you set the padding the more you see of prev & next page
        viewpager.setPadding(70, 0, 70, 0);
        viewpager.setPageMargin(30);
        viewpager.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("OnPostResume", "onResume");
        userId = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.ID);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getSlideImageList(slideImageMainCallback);
            RetrofitHelper.getInstance().getUnreadNotificationOnly(unreadNotificationCallback, userId);
            RetrofitHelper.getInstance().getCategoryList(tableViewImageCallback, userId);


        }
        ṁNavigationNameEmailSet();
        callEvery4SecondForNotification();
    }

    //Handler for call every four second in android
    Handler handler1 = new Handler();
    int delay1 = 3000; //milliseconds

    public void callEvery4SecondForNotification() {
        handler1.postDelayed(new Runnable() {
            public void run() {
                //do something
                handler1.postDelayed(this, delay1);
                RetrofitHelper.getInstance().getUnreadNotificationOnly(unreadNotificationCallback, userId);
            }
        }, delay1);
    }

    //This class is used for Autoscroll the images added in viewPager
    public class FixedSpeedScroller extends Scroller {

        private int mDuration = 5000;

        public FixedSpeedScroller(Context context) {
            super(context);
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator) {
            super(context, (android.view.animation.Interpolator) interpolator);
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator, boolean flywheel) {
            super(context, (android.view.animation.Interpolator) interpolator, flywheel);
        }


        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }
    }


    //ViewPager for show All screens
    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    int viewpageCurrentPage = 0;
    int currentPage = 0;
    final int duration = 5000;
    final int pixelsToMove = 25;
    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private final Runnable SCROLLING_RUNNABLE = new Runnable() {

        @Override
        public void run() {
            //recyclerview_slides.smoothScrollBy(pixelsToMove, 0);
            mHandler.postDelayed(this, duration);
            //currentPage=viewpager.getCurrentItem();
            //Log.e("viewPager=", currentPage + "     " + ((viewpager.getCurrentItem()) + 1));
            viewPagerAutoScroll(bannerSize);
            //Log.e("viewPager=",currentPage+"     "+((viewpager.getCurrentItem())+1));
            //Log.e("autoCalled=","autoCalled");
            //recyclerview_slides.smoothScrollToPosition(slideTopAdapter.getItemCount() - 1);
        }
    };

    private void viewPagerAutoScroll(int Size) {
        viewpageCurrentPage = ((viewpager.getCurrentItem()) + 1);
        if (viewpageCurrentPage == Size) {
            viewpageCurrentPage = 0;
        }
        if (currentPage != viewpageCurrentPage) {
            currentPage = viewpager.getCurrentItem();
        }

        if (currentPage != 0) {
            viewpager.setCurrentItem(currentPage++, true);
        } else {
            viewpager.setCurrentItem(currentPage++, false);
        }
        if (currentPage == Size) {
            currentPage = 0;
        }
    }

    public class SlideTopAdapter extends RecyclerView.Adapter<SlideTopAdapter.ViewHolder> {


        public SlideTopAdapter() {

        }

        @Override
        public SlideTopAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.list_tips_main, parent, false);
            SlideTopAdapter.ViewHolder viewHolder = new SlideTopAdapter.ViewHolder(listItem);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(SlideTopAdapter.ViewHolder holder, final int position) {
            //holder.tv_tips_text.setText(tipsOfTheDayText);
           /* holder.tv_tips_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });*/


        }


        @Override
        public int getItemCount() {
            return 30;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView tv_tips_text;

            public ViewHolder(View itemView) {
                super(itemView);
                //tv_tips_text = itemView.findViewById(R.id.tv_tips_text);


            }
        }
    }

    private View.OnClickListener imgView_navigationIconListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        }
    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_my_notification) {
            //Intent myNotification = new Intent(getApplicationContext(), MyNotification.class);
            Intent myNotification = new Intent(getApplicationContext(), Notification.class);
            startActivity(myNotification);
            // Handle the camera action
        } else if (id == R.id.nav_mypassbook) {
            Intent mypassbook = new Intent(getApplicationContext(), MyPassbook.class);
            startActivity(mypassbook);

        } else if (id == R.id.nav_profile) {
            Intent profileActivity = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(profileActivity);

        } else if (id == R.id.nav_myfavouritestores) {
            Intent myFavourite = new Intent(getApplicationContext(), MyFavourite.class);
            startActivity(myFavourite);

        } else if (id == R.id.nav_setting) {
            Intent setting = new Intent(getApplicationContext(), Settings.class);
            startActivity(setting);

        } else if (id == R.id.nav_logout) {
            FragmentManager manager = getSupportFragmentManager();
            MyDialogFragment alertDialogFragment = new MyDialogFragment();
            alertDialogFragment.show(manager, "fragment_edit_name");
        }else if (id == R.id.nav_socialLinks) {
            Intent setting = new Intent(getApplicationContext(), SocialLinks.class);
            startActivity(setting);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @OnClick(R.id.layout_unreadNotification)
    void unreadNotification(View view) {
        if (countNotificaton>0){
            Intent unreadNotificationIntent = new Intent(getApplicationContext(), UnreadNotification.class);
            startActivity(unreadNotificationIntent);
        }else{
           Snackbar.make(layout_unreadNotification,getResources().getString(R.string.not_notification),Snackbar.LENGTH_LONG).show();

        }

    }


    //All Callbacks
    Callback<SlideImageMain> slideImageMainCallback = new Callback<SlideImageMain>() {

        @Override
        public void onResponse(Call<SlideImageMain> call, Response<SlideImageMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                ArrayList<SliderImageMainData> slideImageData = new ArrayList<>();
                slideImageData.clear();
                slideImageData.addAll(response.body().getData());
                addTabs(response.body().getData().size(), slideImageData);
                bannerSize = response.body().getData().size();
            }
        }

        @Override
        public void onFailure(Call<SlideImageMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    Callback<UnreadCountMain> unreadNotificationCallback = new Callback<UnreadCountMain>() {

        @Override
        public void onResponse(Call<UnreadCountMain> call, Response<UnreadCountMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess() == 1) {
                    imgview_badge.setBadgeValue(response.body().getCount());
                    countNotificaton=response.body().getCount();
                }else{
                    imgview_badge.setBadgeValue(0);
                    countNotificaton=0;
                }

            }
        }

        @Override
        public void onFailure(Call<UnreadCountMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    Callback<CategoryListMain> tableViewImageCallback = new Callback<CategoryListMain>() {
        @Override
        public void onResponse(Call<CategoryListMain> call, Response<CategoryListMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess() == 1) {
                    categoryListMainData = new ArrayList<>();
                    categoryListMainData.clear();
                    categoryListMainData.addAll(response.body().getData());
                    categoryListAdapterMainActivity = new CategoryListAdapterMainActivity(getApplicationContext(), categoryListMainData);
                    recyclerview_categoryList.setAdapter(categoryListAdapterMainActivity);
                } else {

                }

            }
        }

        @Override
        public void onFailure(Call<CategoryListMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
}
