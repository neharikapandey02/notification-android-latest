package com.frcoder.notification.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.frcoder.notification.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyPassbook extends AppCompatActivity {
    @BindView(R.id.imgView_navigationIcon1)
    ImageView imgView_navigationIcon1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_my_passbook);
        ButterKnife.bind(this);
    }

    //back imageView click listner for backpressed
    @OnClick(R.id.imgView_navigationIcon1)
    public void backClick(View view){
        onBackPressed();
    }
}
