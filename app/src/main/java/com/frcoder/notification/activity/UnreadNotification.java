package com.frcoder.notification.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.frcoder.notification.ModelsClasses.UnreadCountMain;
import com.frcoder.notification.ModelsClasses.UnreadCountMainData;
import com.frcoder.notification.R;
import com.frcoder.notification.RetrofitApiCalling.RetrofitHelper;
import com.frcoder.notification.constans.Constant;
import com.frcoder.notification.constans.NetworkUtil;
import com.frcoder.notification.constans.SharedPreferencesData;
import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UnreadNotification extends AppCompatActivity {
    @BindView(R.id.recyclerView_unreadNotification)
    RecyclerView recyclerView_unreadNotification;

    @BindView(R.id.imgView_notificationBack)
    ImageView imgView_notificationBack;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    private UnreadNotificationAdapter unreadNotificationAdapter;
    private ArrayList<UnreadCountMainData> unreadNotificationDataList;

    Locale myLocale;
    private SharedPreferencesData sharedPreferencesData;
    private String userId;

    private String newNotification="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unread_notification2);
        ButterKnife.bind(this);
        viewFinds();
    }



    private void viewFinds() {
         /* Bundle extras = getIntent().getExtras();
          if (extras!=null){
              newNotification=extras.getString(NOTIFICATIONNEW);
              Log.e("TAG=","NEW_NOFITIFICATION="+newNotification);
          }*/



        //setting of recyclerview
        recyclerView_unreadNotification.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, true);
        layoutManager.setReverseLayout(false);
        recyclerView_unreadNotification.setLayoutManager(layoutManager);
        unreadNotificationAdapter = new UnreadNotificationAdapter();
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        userId=sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE,Constant.ID);

        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getUnreadNotificationOnly(unreadNotificationCallback,userId);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        userId=sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE,Constant.ID);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
           // progressBar.setVisibility(View.VISIBLE);
            //RetrofitHelper.getInstance().getUnreadNotificationOnly(unreadNotificationCallback,userId);
        }
    }

    //All ClickListner Called Below
    @OnClick(R.id.imgView_notificationBack)
    void onBackClick(View view) {
        onBackPressed();
    }


    //This is the adapter tha show all unread notification come from server
    public class UnreadNotificationAdapter extends RecyclerView.Adapter<UnreadNotificationAdapter.ViewHolder> {


        public UnreadNotificationAdapter() {

        }

        @Override
        public UnreadNotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.item_unread_notification, parent, false);
            UnreadNotificationAdapter.ViewHolder viewHolder = new UnreadNotificationAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(UnreadNotificationAdapter.ViewHolder holder, final int position) {
            //String date = serverDateToLocalDate(unreadNotificationDataList.get(position).getDate());
            holder.tv_date.setText(unreadNotificationDataList.get(position).getStartDate());
            Log.e("date=", serverDateToLocalDate(unreadNotificationDataList.get(position).getStartDate()));
            holder.tv_offerType.setText(unreadNotificationDataList.get(position).getTitle());
            holder.layoutLinear_offer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), NotificationDetails.class);
                    intent.putExtra(Constant.URL, unreadNotificationDataList.get(position).getImage());
                    intent.putExtra(Constant.START_DATE, unreadNotificationDataList.get(position).getStartDate());
                    intent.putExtra(Constant.END_DATE, unreadNotificationDataList.get(position).getEndDate());
                    intent.putExtra(Constant.DETAILS, unreadNotificationDataList.get(position).getContent());
                    intent.putExtra(Constant.NOTIFICATIONID, unreadNotificationDataList.get(position).getId());
                    intent.putExtra(Constant.FAVOURITESTATUS, unreadNotificationDataList.get(position).getFavourite());
                    intent.putExtra(Constant.TITLE, unreadNotificationDataList.get(position).getTitle());
                    intent.putExtra(Constant.MESSAGEID, unreadNotificationDataList.get(position).getId());
                    startActivity(intent);
                }
            });
        }


        @Override
        public int getItemCount() {
            return unreadNotificationDataList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView tv_tips_text, tv_date, tv_offerType;
            private LinearLayout layoutLinear_offer;

            public ViewHolder(View itemView) {
                super(itemView);
                tv_date = itemView.findViewById(R.id.tv_date);
                tv_offerType = itemView.findViewById(R.id.tv_offerType);
                layoutLinear_offer = itemView.findViewById(R.id.layoutLinear_offer);

            }
        }
    }

    //Date Formater server date to local date format
    private String serverDateToLocalDate(String serverDate) {
        String strCurrentDate = serverDate;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate = null;
        String date = "";
        try {
            newDate = format.parse(strCurrentDate);
            format = new SimpleDateFormat("dd MMM,yyyy");
            date = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;

    }


    //All Callbacks here
    Callback<UnreadCountMain> unreadNotificationCallback = new Callback<UnreadCountMain>() {

        @Override
        public void onResponse(Call<UnreadCountMain> call, Response<UnreadCountMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()==1){
                        unreadNotificationDataList = new ArrayList<>();
                        unreadNotificationDataList.clear();
                        unreadNotificationDataList.addAll(response.body().getNotificationList());
                        recyclerView_unreadNotification.setAdapter(unreadNotificationAdapter);

                    }else{
                        Snackbar.make(recyclerView_unreadNotification,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                    }


            }
        }

        @Override
        public void onFailure(Call<UnreadCountMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
}
