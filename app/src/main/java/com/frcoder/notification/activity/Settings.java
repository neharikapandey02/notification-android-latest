package com.frcoder.notification.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.frcoder.notification.ModelsClasses.CategoryListUserMain;
import com.frcoder.notification.ModelsClasses.CategoryListUserMainData;
import com.frcoder.notification.R;
import com.frcoder.notification.RetrofitApiCalling.RetrofitHelper;
import com.frcoder.notification.constans.Constant;
import com.frcoder.notification.constans.NetworkUtil;
import com.frcoder.notification.constans.SharedPreferencesData;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Settings extends AppCompatActivity {
    @BindView(R.id.imgView_navigationIcon1)
    ImageView imgView_navigationIcon1;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.recyclerView_category_user)
    RecyclerView recyclerView_category_user;


    @BindView(R.id.btn_updateCategory)
    Button btn_updateCategory;

    private SharedPreferencesData sharedPreferencesData;
    private String userId;
    private List<CategoryListUserMainData> categoryListUserMainDataList;
    private RecyclerView.LayoutManager layoutManager;
    private String category="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        userId=sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE,Constant.ID);

        recyclerView_category_user.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerView_category_user.setLayoutManager(layoutManager);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getUserCategoryList(getCategoryCallback,userId);
        }
    }

    Callback<CategoryListUserMain> getCategoryCallback = new Callback<CategoryListUserMain>() {
        @Override
        public void onResponse(Call<CategoryListUserMain> call, Response<CategoryListUserMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess()==1){
                    if (response.body()!=null){
                        categoryListUserMainDataList=new ArrayList<>();
                        categoryListUserMainDataList.clear();
                        categoryListUserMainDataList.addAll(response.body().getUserCategory());
                        CategoryUserListAdapter categoryUserListAdapter=new CategoryUserListAdapter(getApplicationContext(),categoryListUserMainDataList);
                        recyclerView_category_user.setAdapter(categoryUserListAdapter);
                    }

                }else{
                    Snackbar.make(recyclerView_category_user,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }

        }

        @Override
        public void onFailure(Call<CategoryListUserMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    Callback<CategoryListUserMain> getCategoryUpdateCallback = new Callback<CategoryListUserMain>() {
        @Override
        public void onResponse(Call<CategoryListUserMain> call, Response<CategoryListUserMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess()==1){
                    if (response.body()!=null){
                        Snackbar.make(recyclerView_category_user,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                    }

                }else{
                    Snackbar.make(recyclerView_category_user,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }

        }

        @Override
        public void onFailure(Call<CategoryListUserMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    //All ClickListner Here
    @OnClick(R.id.imgView_navigationIcon1)
    void backListner(View v){
        onBackPressed();
    }


    @OnClick(R.id.btn_updateCategory)
    void onClickButtonUpdateCategory(View v){
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);

            for (int i = 0; i < categoryCheckedArrayList.size(); i++) {
                if(categoryCheckedArrayList.get(i).getSetCheckedValue()){
                    category =categoryListUserMainDataList.get(i).getId() + "," +category;
                }

            }
            if(category.endsWith(""))
            {
                if (category.length()>1) {
                    category = category.substring(0, category.length() - 1);
                }
            }
            RetrofitHelper.getInstance().getUserCategoryUpdate(getCategoryUpdateCallback,userId,category);
        }
    }



    private ArrayList<CategoryChecked> categoryCheckedArrayList;
    class CategoryUserListAdapter extends RecyclerView.Adapter<CategoryUserListAdapter.MyViewHolder> {
        private int adapterPosition = -1;
        private Context context;
        private List<CategoryListUserMainData> categoryListUserMainData;


        public CategoryUserListAdapter(Context context,List<CategoryListUserMainData> categoryListUserMainData) {
            this.context=context;
            this.categoryListUserMainData=categoryListUserMainData;
            categoryCheckedArrayList=new ArrayList<>();
            for (int i=0;i<categoryListUserMainData.size();i++){

                CategoryChecked categoryChecked = new CategoryChecked();
                if (categoryListUserMainData.get(i).getUserSelected().equalsIgnoreCase("true")){
                    categoryChecked.setSetCheckedValue(true);
                }else{
                    categoryChecked.setSetCheckedValue(false);
                }

                categoryChecked.setCategoryId(categoryListUserMainData.get(i).getId());
                categoryCheckedArrayList.add(categoryChecked);

            }


        }

        @NonNull
        @Override
        public CategoryUserListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_interested_category, parent, false);
            return new CategoryUserListAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CategoryUserListAdapter.MyViewHolder holder, final int position) {
            holder.tv_category.setText(categoryListUserMainData.get(position).getCategoryName());
            Picasso.with(context)
                    .load(categoryListUserMainData.get(position).getBig())
                    .placeholder(R.drawable.airtel_icon)
                    .error(R.drawable.airtel_icon)
                    .into(holder.imgV_notification);
            if (categoryCheckedArrayList.get(position).getSetCheckedValue()) {
                holder.switch_category.setChecked(true);
                Log.e("autoCalled=", "InsideTrue");

            } else {
                holder.switch_category.setChecked(false);
                Log.e("autoCalled1=", "InsideFalse");
            }


        }

        @Override
        public int getItemCount() {
            return categoryListUserMainData.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView tv_category;
            private Switch switch_category;
            private ImageView imgV_notification;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_category = itemView.findViewById(R.id.tv_category);
                switch_category = itemView.findViewById(R.id.switch_category);
                imgV_notification = itemView.findViewById(R.id.imgV_notification);
                switch_category.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        CategoryChecked categoryChecked = new CategoryChecked();
                        categoryChecked.setSetCheckedValue(isChecked);
                        categoryChecked.setCategoryName(categoryListUserMainData.get(getAdapterPosition()).getCategoryName());
                        categoryCheckedArrayList.set(getAdapterPosition(), categoryChecked);
                        Log.e("size=", categoryListUserMainData.size() + "   " + getAdapterPosition());

                    }

                });

            }
        }
    }
}
