package com.frcoder.notification.activity;

import android.content.res.Resources;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.frcoder.notification.R;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyNotification extends AppCompatActivity {
    @BindView(R.id.recyclerView_unreadNotification)
    RecyclerView recyclerView_unreadNotification;

    @BindView(R.id.imgView_navigationIcon1)
    ImageView imgView_navigationIcon1;


    private MyNotificationAdapter myNotificationAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_my_notification);
        ButterKnife.bind(this);

        Log.e("CurrentLangauge=", Locale.getDefault().getDisplayLanguage());
        Log.e("GetSystemLanguage=", Resources.getSystem().getConfiguration().locale.getLanguage());
        Log.e("GetAppLanguage=", Locale.getDefault().getLanguage());
        viewFinds();
    }

    private void viewFinds() {
        recyclerView_unreadNotification.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, true);
        layoutManager.setReverseLayout(false);
        recyclerView_unreadNotification.setLayoutManager(layoutManager);
        myNotificationAdapter = new MyNotificationAdapter();
        recyclerView_unreadNotification.setAdapter(myNotificationAdapter);
    }
    public class MyNotificationAdapter extends RecyclerView.Adapter<MyNotificationAdapter.ViewHolder> {


        public MyNotificationAdapter() {

        }

        @Override
        public MyNotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.items_my_notification, parent, false);
            MyNotificationAdapter.ViewHolder viewHolder = new MyNotificationAdapter.ViewHolder(listItem);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(MyNotificationAdapter.ViewHolder holder, final int position) {
            //String date = serverDateToLocalDate(unreadNotificationDataList.get(position).getDate());


        }


        @Override
        public int getItemCount() {
            return 20;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView tv_tips_text, tv_date, tv_offerType;
            private LinearLayout layoutLinear_offer;

            public ViewHolder(View itemView) {
                super(itemView);


            }
        }
    }

    //All Click Listner here
    @OnClick(R.id.imgView_navigationIcon1)
    void backClick(View v) {
        onBackPressed();
    }

}
