package com.frcoder.notification.activity;

import android.content.Intent;
import android.provider.CalendarContract;

import com.frcoder.notification.ModelsClasses.LastNotificatinMain;
import com.frcoder.notification.ModelsClasses.MakeReadMain;
import com.frcoder.notification.R;
import com.frcoder.notification.constans.URLs;
import com.frcoder.notification.ModelsClasses.AddFavouriteMain;
import com.frcoder.notification.RetrofitApiCalling.RetrofitHelper;
import com.frcoder.notification.constans.Constant;
import com.frcoder.notification.constans.NetworkUtil;
import com.frcoder.notification.constans.SharedPreferencesData;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationDetails extends AppCompatActivity {
    @BindView(R.id.imgView_notificationBack1)
    ImageView imgView_notificationBack;


    @BindView(R.id.imgV_offer)
    ImageView imgV_offer;

    @BindView(R.id.tv_offerFrom)
    TextView tv_offerFrom;


    @BindView(R.id.tv_offerTo)
    TextView tv_offerTo;


    @BindView(R.id.tv_message)
    TextView tv_message;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.btn_favourite)
    Button btn_favourite;

    @BindView(R.id.btn_addToCalender)
    Button btn_addToCalender;

    private String urls,offerfrom,offerto,details;
    private String userId,notificationId,favouriteStatus;
    private SharedPreferencesData sharedPreferencesData;
    private String startDate[];
    private String endDate[];
    private String title;
    private String userMailId;
    private String mesageId;
    private String notificationIdfromNotification;

    private String newNotification="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_details);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {

        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        userId=sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE,Constant.ID);
        Bundle extras = getIntent().getExtras();
        if (extras!=null&&extras.getString(Constant.NOTIFICATIONNEW)!=null){
            newNotification=extras.getString(Constant.NOTIFICATIONNEW);
            Log.e("TAG=","NEW_NOFITIFICATION="+newNotification);
            notificationIdfromNotification=extras.getString(Constant.NOTIFICATIONNEW);
            comeFromNotificationClicked();
        }else{
            comeFromListClicked();

        }







    }

    private void comeFromListClicked() {

        favouriteStatus=getIntent().getStringExtra(Constant.FAVOURITESTATUS);
        if (favouriteStatus!=null&&!favouriteStatus.equalsIgnoreCase("")){
            if (favouriteStatus.equalsIgnoreCase("1")){
                btn_favourite.setText(getResources().getString(R.string.unfavourite));
                favouriteStatus="2";

            }else if (favouriteStatus.equalsIgnoreCase("2")){
                btn_favourite.setText(getResources().getString(R.string.favourite));
                favouriteStatus="1";
            }else{
                favouriteStatus="1";
                btn_favourite.setText(getResources().getString(R.string.favourite));
            }
        }else{
            favouriteStatus="1";
            btn_favourite.setText(getResources().getString(R.string.favourite));
        }
        mesageId = getIntent().getStringExtra(Constant.MESSAGEID);
        urls = getIntent().getStringExtra(Constant.URL);
        offerfrom = getIntent().getStringExtra(Constant.START_DATE);
        offerto = getIntent().getStringExtra(Constant.END_DATE);
        details = getIntent().getStringExtra(Constant.DETAILS);
        notificationId = getIntent().getStringExtra(Constant.NOTIFICATIONID);
        title = getIntent().getStringExtra(Constant.TITLE);
        Picasso.with(getApplicationContext())
                .load(urls)
                .placeholder(R.drawable.airtel_icon)
                .error(R.drawable.airtel_icon)
                .into(imgV_offer);
        tv_offerFrom.setText(offerfrom);
        tv_offerTo.setText(offerto);
        tv_message.setText(details);

        userMailId=sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE,Constant.EMAIL);
        if (userMailId!=null){

        }else{
            userMailId="www.notification.com";
        }

        startDate=offerfrom.split("-");
        endDate=offerto.split("-");
    }

    private void comeFromNotificationClicked() {
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            RetrofitHelper.getInstance().getLastNotificationDetails(getLastNotificationCallback,newNotification);
        }else{
            Snackbar.make(tv_message,getResources().getString(R.string.no_network_found),Snackbar.LENGTH_LONG).show();
        }




    }

    Callback<LastNotificatinMain> getLastNotificationCallback = new Callback<LastNotificatinMain>() {
        @Override
        public void onResponse(Call<LastNotificatinMain> call, Response<LastNotificatinMain> response) {
            //progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess()==1) {
                    favouriteStatus=response.body().getNotificationList().get(0).getFavourite();
                    if (favouriteStatus!=null&&!favouriteStatus.equalsIgnoreCase("")){
                        if (favouriteStatus.equalsIgnoreCase("1")){
                            btn_favourite.setText(getResources().getString(R.string.unfavourite));
                            favouriteStatus="2";

                        }else if (favouriteStatus.equalsIgnoreCase("2")){
                            btn_favourite.setText(getResources().getString(R.string.favourite));
                            favouriteStatus="1";
                        }else{
                            favouriteStatus="1";
                            btn_favourite.setText(getResources().getString(R.string.favourite));
                        }
                    }else{
                        favouriteStatus="1";
                        btn_favourite.setText(getResources().getString(R.string.favourite));
                    }
                    mesageId = response.body().getNotificationList().get(0).getId();
                    urls = response.body().getNotificationList().get(0).getImage();
                    offerfrom = response.body().getNotificationList().get(0).getStartDate();
                    offerto = response.body().getNotificationList().get(0).getEndDate();
                    details = response.body().getNotificationList().get(0).getContent();
                    notificationId = response.body().getNotificationList().get(0).getNotificationId();
                    title = response.body().getNotificationList().get(0).getTitle();
                    Picasso.with(getApplicationContext())
                            .load(urls)
                            .placeholder(R.drawable.airtel_icon)
                            .error(R.drawable.airtel_icon)
                            .into(imgV_offer);
                    tv_offerFrom.setText(offerfrom);
                    tv_offerTo.setText(offerto);
                    tv_message.setText(details);
                    userId=sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE,Constant.ID);
                    userMailId=sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE,Constant.EMAIL);
                    if (userMailId!=null){

                    }else{
                        userMailId="www.notification.com";
                    }

                    startDate=offerfrom.split("-");
                    endDate=offerto.split("-");
                }
            }


        }

        @Override
        public void onFailure(Call<LastNotificatinMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        viewReadMessageCallback();
    }

    private void viewReadMessageCallback() {
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            RetrofitHelper.getInstance().setMakeReadNotification(setMakeReadCallback,mesageId,userId);
        }
    }

    Callback<MakeReadMain> setMakeReadCallback = new Callback<MakeReadMain>() {
        @Override
        public void onResponse(Call<MakeReadMain> call, Response<MakeReadMain> response) {
            //progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess()==1) {
                }
            }


        }

        @Override
        public void onFailure(Call<MakeReadMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };



    Callback<AddFavouriteMain> setMakeFavouriteCallback = new Callback<AddFavouriteMain>() {
        @Override
        public void onResponse(Call<AddFavouriteMain> call, Response<AddFavouriteMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess()==1){
                    if (favouriteStatus.equalsIgnoreCase("1")){
                        favouriteStatus="2";
                        btn_favourite.setText(getResources().getString(R.string.unfavourite));
                    }else{
                        favouriteStatus="1";
                        btn_favourite.setText(getResources().getString(R.string.favourite));
                    }
                    Snackbar.make(btn_favourite,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }else{
                    Snackbar.make(btn_favourite,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }


        }

        @Override
        public void onFailure(Call<AddFavouriteMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    //back imageView click listner for backpressed



    //All onClick Listner
    @OnClick(R.id.imgView_notificationBack1)
    public void onClickBack(View view) {
        if(newNotification.equalsIgnoreCase("")){
            onBackPressed();
        }else{
           Intent intent=new Intent(getApplicationContext(),MainActivity.class);
           startActivity(intent);
           finish();
        }

    }

    @Override
    public void onBackPressed() {
        if(newNotification.equalsIgnoreCase("")){
            super.onBackPressed();
        }else{
            Intent intent=new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @OnClick(R.id.btn_share)
    public void onClickShare(View view) {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Notification");
            String shareMessage= URLs.BASE_URL+"notification-detail.php?id="+notificationId+"\n\n";
            //shareMessage = shareMessage;
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URLs.BASE_URL+"notification-detail.php?id="+notificationId));
            startActivity(browserIntent);*/
        } catch(Exception e) {
            //e.toString();
        }
    }


    @OnClick(R.id.btn_addToCalender)
    public void onCalenderAdd(View view) {
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(Integer.parseInt(startDate[2]), Integer.parseInt(startDate[1])-1, Integer.parseInt(startDate[0]), 12, 00);
        Calendar endTime = Calendar.getInstance();
        endTime.set(Integer.parseInt(endDate[2]), Integer.parseInt(endDate[1])-1, Integer.parseInt(endDate[0]), 12, 00);
        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, title)
                .putExtra(CalendarContract.Events.DESCRIPTION, details)
                .putExtra(CalendarContract.Events.EVENT_LOCATION, getResources().getString(R.string.from_notification))
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                .putExtra(Intent.EXTRA_EMAIL, userMailId);
                 startActivity(intent);
    }


    @OnClick(R.id.btn_favourite)
    public void onClickFavourite(View view) {
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            //favouritestatus 1=favourite 2=unfavourite

            RetrofitHelper.getInstance().setMakeFafouriteNotification(setMakeFavouriteCallback,userId,notificationId,favouriteStatus);
        }
    }


}
