package com.frcoder.notification.activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.frcoder.notification.R;
import com.frcoder.notification.constans.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BasicInformationActivity extends AppCompatActivity {
    @BindView(R.id.btn_Skip)
    Button btn_Skip;

    @BindView(R.id.btn_Next)
    Button btn_Next;

    @BindView(R.id.et_firstName)
    EditText et_firstName;

    @BindView(R.id.et_lastName)
    EditText et_lastName;

    private String mobileNumber = "", gender = "", firstName = "", lastName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        setContentView(R.layout.activity_basic_information);
        ButterKnife.bind(this);
        viewFinds();

    }


    private void viewFinds() {
        mobileNumber = getIntent().getStringExtra(Constant.MOBILE_NUMBER);
        gender = getIntent().getStringExtra(Constant.GENDER);
        btn_Skip.setOnClickListener(btn_SkipListner);
        btn_Next.setOnClickListener(btn_NextListner);
    }

    //All ClickListener below
    private final View.OnClickListener btn_SkipListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), InterestedCategoryActivity.class);
            intent.putExtra(Constant.MOBILE_NUMBER, mobileNumber);
            intent.putExtra(Constant.GENDER, gender);
            intent.putExtra(Constant.FIRST_NAME, "");
            intent.putExtra(Constant.LAST_NAME, "");
            startActivity(intent);
        }
    };

    private final View.OnClickListener btn_NextListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), InterestedCategoryActivity.class);
            intent.putExtra(Constant.MOBILE_NUMBER, mobileNumber);
            intent.putExtra(Constant.GENDER, gender);
            intent.putExtra(Constant.FIRST_NAME, et_firstName.getText().toString());
            intent.putExtra(Constant.LAST_NAME, et_lastName.getText().toString());
            startActivity(intent);
        }
    };
}
