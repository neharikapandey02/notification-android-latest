package com.frcoder.notification.activity;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.frcoder.notification.R;
import com.frcoder.notification.constans.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenderActivity extends AppCompatActivity {
    @BindView(R.id.btn_Next)
    Button btn_Next;

    @BindView(R.id.radioGroup_Gender)
    RadioGroup radioGroup_Gender;

    @BindView(R.id.radioButton_Male)
    RadioButton radioButton_Male;

    @BindView(R.id.radioButton_Female)
    RadioButton radioButton_Female;

    @BindView(R.id.radioButton_dontWant)
    RadioButton radioButton_dontWant;

    private String mobileNumber;
    private String gender = "male";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        setContentView(R.layout.activity_gender);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        mobileNumber = getIntent().getStringExtra(Constant.MOBILE_NUMBER);
        btn_Next.setOnClickListener(btn_NextListner);
        radioGroup_Gender.setOnCheckedChangeListener(radioGroup_GenderListner);
    }

    //All ClickListners
    private View.OnClickListener btn_NextListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), BasicInformationActivity.class);
            intent.putExtra(Constant.MOBILE_NUMBER, mobileNumber);
            intent.putExtra(Constant.GENDER, gender);
            startActivity(intent);
        }
    };
    private RadioGroup.OnCheckedChangeListener radioGroup_GenderListner = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == R.id.radioButton_Male) {
                gender = "male";
            } else if (checkedId == R.id.radioButton_Female) {

                    gender = "female";

            }else if (checkedId == R.id.radioButton_dontWant){
                gender = "not specified";
            }

        }
    };
}
