package com.frcoder.notification.activity;

public class CategoryChecked {
    public boolean getSetCheckedValue() {
        return setCheckedValue;
    }

    public void setSetCheckedValue(boolean setCheckedValue) {
        this.setCheckedValue = setCheckedValue;
    }

    private boolean setCheckedValue;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    private String categoryName;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    private String categoryId;

}
