package com.frcoder.notification.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frcoder.notification.ModelsClasses.CategoryWiseNotificationMain;
import com.frcoder.notification.ModelsClasses.CategoryWiseNotificationMainData;
import com.frcoder.notification.R;
import com.frcoder.notification.RetrofitApiCalling.RetrofitHelper;
import com.frcoder.notification.adapters.CateogriesListDetailsAdapter;
import com.frcoder.notification.constans.Constant;
import com.frcoder.notification.constans.NetworkUtil;
import com.frcoder.notification.constans.SharedPreferencesData;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryListDetails extends AppCompatActivity {
    @BindView(R.id.recyclerView_categoryListDetails)
    RecyclerView recyclerView_categoryListDetails;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.imgView_navigationIcon1)
    ImageView imgView_navigationIcon1;

    @BindView(R.id.tv_categoryName)
    TextView tv_categoryName;

    private CateogriesListDetailsAdapter cateogriesListDetailsAdapter;
    private String userId,categoryId,categoryName;
    private SharedPreferencesData sharedPreferencesData;

    private List<CategoryWiseNotificationMainData> categoryWiseNotificationMainData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list_details);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        userId=sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE,Constant.ID);
        categoryId=getIntent().getStringExtra(Constant.CATEGORYID);
        categoryName=getIntent().getStringExtra(Constant.CATEGORY_NAME);
        tv_categoryName.setText(categoryName);
        recyclerView_categoryListDetails.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, true);
        layoutManager.setReverseLayout(false);
        recyclerView_categoryListDetails.setLayoutManager(layoutManager);

        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().setCategoryWiseList(setCategoryWiseCallback,userId,categoryId);
        }





    }



    Callback<CategoryWiseNotificationMain> setCategoryWiseCallback = new Callback<CategoryWiseNotificationMain>() {
        @Override
        public void onResponse(Call<CategoryWiseNotificationMain> call, Response<CategoryWiseNotificationMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess()==1){
                    if (response.body()!=null){
                        categoryWiseNotificationMainData=new ArrayList<>();
                        categoryWiseNotificationMainData.clear();
                        categoryWiseNotificationMainData.addAll(response.body().getResult());
                        cateogriesListDetailsAdapter = new CateogriesListDetailsAdapter(getApplicationContext(),categoryWiseNotificationMainData);
                        recyclerView_categoryListDetails.setAdapter(cateogriesListDetailsAdapter);
                    }

                }else{
                    Snackbar.make(recyclerView_categoryListDetails,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }

        }

        @Override
        public void onFailure(Call<CategoryWiseNotificationMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @OnClick(R.id.imgView_navigationIcon1)
    public void backClick(View view){
        onBackPressed();
    }
}
