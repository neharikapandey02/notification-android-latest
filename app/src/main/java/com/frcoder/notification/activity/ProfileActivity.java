package com.frcoder.notification.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.frcoder.notification.ModelsClasses.UserCreateMain;
import com.frcoder.notification.ModelsClasses.UserUpdateData;
import com.frcoder.notification.R;
import com.frcoder.notification.RetrofitApiCalling.RetrofitHelper;
import com.frcoder.notification.constans.Constant;
import com.frcoder.notification.constans.NetworkUtil;
import com.frcoder.notification.constans.RealPathUtil;
import com.frcoder.notification.constans.SharedPreferencesData;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    private String userImage = "";
    private static final int PERMISSION_REQUEST_CODE_CAMERA = 101;
    private static final int PERMISSION_REQUEST_CODE_GALLERY = 102;
    @BindView(R.id.imgView_navigationIcon1)
    ImageView imgView_navigationIcon1;

    @BindView(R.id.et_firstName)
    EditText et_firstName;

    @BindView(R.id.et_lastName)
    EditText et_lastName;

    @BindView(R.id.et_dateOfBirth)
    TextView et_dateOfBirth;

    @BindView(R.id.et_address)
    EditText et_address;

    @BindView(R.id.et_phoneNumber)
    TextView et_phoneNumber;

    @BindView(R.id.et_email)
    EditText et_email;


    @BindView(R.id.btn_editProfile)
    Button btn_editProfile;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.img_profile)
    CircularImageView img_profile;

    private Dialog dialog;

    //ActivityProfileBinding activityProfileBinding;

    private SharedPreferencesData sharedPreferencesData;
    private String userId, mobileNumber = "", gender = "", firstName = "", lastName = "", category = "", dateOfBirth = "", address = "", email = "", image = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_profile);
         //Created by itself for DATA BINDING PATTERN
        //activityProfileBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        //Using Data Binding Pattern MVVM
        UserUpdateData userCreateData = new UserUpdateData();


        //shared Prefernces data getting
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        mobileNumber = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.MOBILE_NUMBER);
        gender = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.GENDER);
        firstName = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.FIRST_NAME);
        lastName = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.LAST_NAME);
        category = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.CATEGORY);
        dateOfBirth = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.DATE_OF_BIRTH);
        address = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.ADDRESS);
        email = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.EMAIL);
        image = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.USERIMAGE);
        userId = sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE, Constant.ID);
        Log.e("userid",userId);


        //data binding model create to set data on UI in xml
        /*userCreateData.setMobileNumber(mobileNumber);
        userCreateData.setFirstName(firstName);
        userCreateData.setLastName(lastName);
        userCreateData.setDateOfBirth(dateOfBirth);
        userCreateData.setAddress(address);
        userCreateData.setEmail(email);
        activityProfileBinding.setUserCrateData(userCreateData);*/
        //Set Text In all Fields
        et_phoneNumber.setText(mobileNumber);
        et_firstName.setText(firstName);
        et_lastName.setText(lastName);
        et_dateOfBirth.setText(dateOfBirth);
        et_address.setText(address);
        et_email.setText(email);
        if(image!=null&&(!image.equalsIgnoreCase(""))){
            Picasso.with(this)
                    .load(image)
                    .placeholder(R.drawable.user)
                    .error(R.drawable.user)
                    .into(img_profile);
        }



    }

    //All Click Listner Here
    @OnClick(R.id.img_profile)
    public void imageClickListner(View view){
        dialog = new Dialog(ProfileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_select_dialogue);
        TextView tv_gallery = dialog.findViewById(R.id.tv_gallery);
        TextView tv_camera = dialog.findViewById(R.id.tv_camera);
        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED) {
                    requestGallery();
                } else {
                        /*Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, PERMISSION_REQUEST_CODE_GALLERY);*/

                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    startActivityForResult(intent, PERMISSION_REQUEST_CODE_GALLERY);

                }


            }
        });
        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED || ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED) {
                    request();
                } else {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    startActivityForResult(intent, PERMISSION_REQUEST_CODE_CAMERA);

                }
            }
        });

        dialog.show();
    }
    public void request() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE_CAMERA);


    }

    public void requestGallery() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE_GALLERY);


    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        dialog.cancel();
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && requestCode == PERMISSION_REQUEST_CODE_GALLERY) {

           /* Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, PERMISSION_REQUEST_CODE_GALLERY);*/

            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, PERMISSION_REQUEST_CODE_GALLERY);


        } else if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && requestCode == PERMISSION_REQUEST_CODE_CAMERA) {

            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            startActivityForResult(intent, PERMISSION_REQUEST_CODE_CAMERA);


        } else {
            Toast.makeText(getApplicationContext(), "Please allow permission", Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        dialog.cancel();

        if (requestCode == PERMISSION_REQUEST_CODE_CAMERA && data != null) {
            Bitmap image = (Bitmap) data.getExtras().get("data");

            img_profile.setImageBitmap(image);

            Uri tempUri = getImageUri(getApplicationContext(), image);

            // CALL THIS METHOD TO GET THE ACTUAL PATH
            File finalFile = new File(getRealPathFromURI(tempUri));
            userImage = finalFile.toString();
            Log.e("pathOfImage", finalFile + "");


        } else if (requestCode == PERMISSION_REQUEST_CODE_GALLERY && data != null) {
            String realPath = "";
            // SDK < API11
            if (Build.VERSION.SDK_INT < 11) {
                realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                Log.e("pathOfImage", realPath + "");

            }

            // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19) {
                realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                Log.e("pathOfImage", realPath + "");
            }

            // SDK > 19 (Android 4.4)
            else {
                realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                Log.e("pathOfImage", realPath + "");
            }

            userImage = realPath;
            Log.e("pathOfImage1", userImage + "");
            Uri selectedImage = data.getData();
            img_profile.setImageURI(selectedImage);


        }


    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }



    @OnClick(R.id.imgView_navigationIcon1)
    void backClick(View view) {
        onBackPressed();
    }


    @OnClick(R.id.et_dateOfBirth)
    void dateOfBirth(View view) {
        calenderOpen();
    }

    @OnClick(R.id.btn_editProfile)
    void editProfile(View view) {

        String firstName, lastName, dateOfBirth, address, phoneNumber, email, category1;
        firstName = et_firstName.getText().toString();
        lastName = et_lastName.getText().toString();
        dateOfBirth = et_dateOfBirth.getText().toString();
        address = et_address.getText().toString();
        phoneNumber = mobileNumber;
        email = et_email.getText().toString();
        category1 = category;
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().setUpdateProfile(setUpdateProfileCallback,userId, phoneNumber, gender, firstName, lastName, category1, dateOfBirth, address, email,userImage);
        }
    }

    //Method for open Calender for date of birth
    public void calenderOpen() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        et_dateOfBirth.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    //All CallBack here

    Callback<UserCreateMain> setUpdateProfileCallback = new Callback<UserCreateMain>() {
        @Override
        public void onResponse(Call<UserCreateMain> call, Response<UserCreateMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()==1){
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.FIRST_NAME, response.body().getData().getFirstname());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.LAST_NAME, response.body().getData().getLastname());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.GENDER, response.body().getData().getGender());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.MOBILE_NUMBER, response.body().getData().getNumber());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.ADDRESS, response.body().getData().getAddress());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.CATEGORY, response.body().getData().getCategory());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.DATE_OF_BIRTH, response.body().getData().getDob());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.EMAIL, response.body().getData().getEmail());
                    sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.USERIMAGE, response.body().getData().getImage());
                    Snackbar.make(et_dateOfBirth, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                }else{
                    Snackbar.make(btn_editProfile,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }

            }
        }

        @Override
        public void onFailure(Call<UserCreateMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


}
