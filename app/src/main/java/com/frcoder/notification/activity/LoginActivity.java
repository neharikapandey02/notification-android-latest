package com.frcoder.notification.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;

import com.example.easywaylocation.EasyWayLocation;
import com.frcoder.notification.R;
import com.frcoder.notification.constans.GPSTracker;
import com.frcoder.notification.ModelsClasses.OtpDataMain;
import com.frcoder.notification.RetrofitApiCalling.RetrofitHelper;
import com.frcoder.notification.constans.Constant;
import com.frcoder.notification.constans.NetworkUtil;
import com.google.android.gms.location.LocationCallback;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;



import java.io.IOException;
import java.util.List;
import java.util.Locale;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements LocationListener {

    @BindView(R.id.tv_countryCode)
    EditText tv_countryCode;

    @BindView(R.id.et_mobileNumber)
    EditText et_mobileNumber;

    @BindView(R.id.btn_ok)
    Button btn_ok;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    String locale;


    protected LocationManager locationManager;
    protected LocationListener locationListener;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    GPSTracker gpsTracker;
    private double latitude;
    private double longitude;
    EasyWayLocation easyWayLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        //Locale current = getResources().getConfiguration().locale;
        tv_countryCode.setText(getCountryDialCode());
        btn_ok.setOnClickListener(btn_okListner);


       // easyWayLocation = new EasyWayLocation(this, true,this);


        locale= getResources().getConfiguration().locale.getDisplayCountry();
        //Log.d("CountryCodeCheck=",locale);
       /* gpsTracker=new GPSTracker(LoginActivity.this);
        if(checkLocationPermission()){
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            // \n is for new line
        }else{

        }*/
        //Log.e("LocationPermission=",+"")  ;


        checkLocationPermission();

        //locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

    }

    private View.OnClickListener btn_okListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


           /*GPSTracker gpsTracker=new GPSTracker(LoginActivity.this);

            double lat=gpsTracker.getLatitude();
            Log.e("TAG=","Latitudddddd="+gpsTracker.getLatitude());
            Log.e("TAG=","Longitude="+gpsTracker.getLongitude());
            Log.e("TAG=","City="+gpsTracker.getAddressFromLatLong());*/

             /*SmartLocation.with(getApplicationContext()).location()
                    .start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(Location location) {

                        }
                    });*/



             /*GPSTracker gpsTracker=new GPSTracker( LoginActivity.this);
              if (gpsTracker.canGetLocation()){
                  Log.e("TAG=","Latitud="+gpsTracker.getLatitude());
                  Log.e("TAG=","Longitude="+gpsTracker.getLongitude());
              }*/

            //Log.e("TAG=","city="+gpsTracker.getLocality(getApplicationContext()));
            if (et_mobileNumber.getText().length() < 1) {
                Snackbar.make(et_mobileNumber, getResources().getString(R.string.enter_mobile_number), Snackbar.LENGTH_LONG).show();
            } else if (tv_countryCode.getText().toString().equalsIgnoreCase("91")){
                if (et_mobileNumber.getText().length() < 10){
                    Snackbar.make(et_mobileNumber, getResources().getString(R.string.enter_valid_mobile_number), Snackbar.LENGTH_LONG).show();
                }else{
                    if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                    /*FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, new OnSuccessListener<InstanceIdResult>() {
                        @Override
                        public void onSuccess(InstanceIdResult instanceIdResult) {
                            String newToken = instanceIdResult.getToken();
                            Log.e("newToken", newToken);

                        }
                    });*/
                        progressBar.setVisibility(View.VISIBLE);
                        RetrofitHelper.getInstance().getOtpData(getOtpDataCallback, et_mobileNumber.getText().toString());


                    }else{
                        Snackbar.make(et_mobileNumber, getResources().getString(R.string.no_network_found), Snackbar.LENGTH_LONG).show();
                    }
                }
            //) {


            } else {

                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                    /*FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, new OnSuccessListener<InstanceIdResult>() {
                        @Override
                        public void onSuccess(InstanceIdResult instanceIdResult) {
                            String newToken = instanceIdResult.getToken();
                            Log.e("newToken", newToken);

                        }
                    });*/
                    progressBar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().getOtpData(getOtpDataCallback, et_mobileNumber.getText().toString());


                }

            }

        }
    };


    public String getCountryDialCode() {
        String contryId = null;
        String contryDialCode = null;

        TelephonyManager telephonyMngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        contryId = telephonyMngr.getSimCountryIso().toUpperCase();
        String[] arrContryCode = getResources().getStringArray(R.array.DialingCountryCode);
        for (int i = 0; i < arrContryCode.length; i++) {
            String[] arrDial = arrContryCode[i].split(",");
            if (arrDial[1].trim().equals(contryId.trim())) {
                contryDialCode = arrDial[0];
                break;
            }
        }
        return contryDialCode;
    }

    //All Callbacks
    Callback<OtpDataMain> getOtpDataCallback = new Callback<OtpDataMain>() {
        @Override
        public void onResponse(Call<OtpDataMain> call, Response<OtpDataMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                Intent intent = new Intent(getApplicationContext(), OtpVerification.class);
                intent.putExtra(Constant.OTP, response.body().getOtp().toString());
                Log.e("Otp=",response.body().getOtp().toString());
                intent.putExtra(Constant.MOBILE_NUMBER, et_mobileNumber.getText().toString());
               // intent.putExtra(Constant.COMPLETE_STATUS, response.body().getData().getCompleteStatus().toString());
                startActivity(intent);
            }

        }

        @Override
        public void onFailure(Call<OtpDataMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    private LocationCallback locationCallback;

    ///for location permission result
    public void checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);

        }else{
            // Write you code here if permission already given.

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(),"Location Update",Toast.LENGTH_LONG).show();
                    gpsTracker = new GPSTracker(LoginActivity.this);
                    latitude= gpsTracker.getLatitude();
                    longitude= gpsTracker.getLongitude();
                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        //locationManager.requestLocationUpdates(provider, 400, 1, this);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }







    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onLocationChanged(Location location) {
        //Log.e("TAG=","Latitudddddd="+gpsTracker.getLatitude());
        //Log.e("TAG=","Longitude="+gpsTracker.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private String getAddressFromLatLong(double latitude,double longitude){
        String city="";
        if (latitude!=0.0&&longitude!=0.0){
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                city= addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


        return city;
    }
}
