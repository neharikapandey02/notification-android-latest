package com.frcoder.notification.activity;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import com.crashlytics.android.Crashlytics;
import com.frcoder.notification.R;
import com.frcoder.notification.constans.Constant;
import com.frcoder.notification.constans.SharedPreferencesData;

import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 2000;
    SharedPreferencesData sharedPreferencesData;
    String flagValue = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());

        flagValue = sharedPreferencesData.getSharedPreferenceData(Constant.USERLOGIN, Constant.FLAG);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                /* If already login then called MainActivity otherwise LoginActivity(two second delay time)*/
                if (flagValue.equalsIgnoreCase("true")) {
                    Log.e("UserId=",sharedPreferencesData.getSharedPreferenceData(Constant.USERCREATE,Constant.ID));
                    Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(mainIntent);
                    finish();
                } else {
                    Intent loginIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(loginIntent);
                    finish();
                }

            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
