package com.frcoder.notification.activity;

import android.content.Intent;
import androidx.annotation.NonNull;

import com.frcoder.notification.R;
import com.frcoder.notification.constans.GPSTracker;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.frcoder.notification.ModelsClasses.CategoryListMain;
import com.frcoder.notification.ModelsClasses.CategoryListMainData;
import com.frcoder.notification.ModelsClasses.UserMain;
import com.frcoder.notification.RetrofitApiCalling.RetrofitHelper;
import com.frcoder.notification.constans.Constant;
import com.frcoder.notification.constans.NetworkUtil;
import com.frcoder.notification.constans.SharedPreferencesData;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InterestedCategoryActivity extends AppCompatActivity implements Callback<CategoryListMain> {
    @BindView(R.id.recyclerView_category)
    RecyclerView recyclerView_category;

    @BindView(R.id.btn_Skip)
    Button btn_Skip;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<CategoryListMainData> categoryListDataArrayList;
    private ArrayList<CategoryChecked> categoryCheckedArrayList;
    private CategoryListAdapter categoryListAdapter;
    private String mobileNumber = "", gender = "", firstName = "", lastName = "";
    String category = "";
    String newToken;
    private SharedPreferencesData sharedPreferencesData;
    private String userId;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        setContentView(R.layout.activity_interested_category);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {


        //for getting fcm token for notification
        //deviceType 1 for android 2 for ios
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(InterestedCategoryActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken= instanceIdResult.getToken();
                Log.e("newToken", newToken);

            }
        });

        //previousDataStored from activity
        mobileNumber = getIntent().getStringExtra(Constant.MOBILE_NUMBER);
        gender = getIntent().getStringExtra(Constant.GENDER);
        firstName = getIntent().getStringExtra(Constant.FIRST_NAME);
        lastName = getIntent().getStringExtra(Constant.LAST_NAME);


        recyclerView_category.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerView_category.setLayoutManager(layoutManager);
        categoryListAdapter = new CategoryListAdapter();

        btn_Skip.setOnClickListener(btn_SkipListner);


        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            RetrofitHelper.getInstance().getCategoryList(InterestedCategoryActivity.this,"");
            progressBar.setVisibility(View.VISIBLE);
        } else {
            Snackbar.make(recyclerView_category, getResources().getString(R.string.no_network_found), Snackbar.LENGTH_LONG).show();
        }





    }


    private View.OnClickListener btn_SkipListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            for (int i = 0; i < categoryCheckedArrayList.size(); i++) {
                if(categoryCheckedArrayList.get(i).getSetCheckedValue()){
                    category =categoryListDataArrayList.get(i).getId() + "," +category;
                }

            }
            if(category.endsWith(""))
            {
                if (category.length()>1){
                    category = category.substring(0,category.length() - 1);
                }

            }




            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                GPSTracker gpsTracker=new GPSTracker( InterestedCategoryActivity.this);
                String city= gpsTracker.getAddressFromLatLong();

                Log.e("TAG=","Latitude="+gpsTracker.getLatitude());
                Log.e("TAG=","Longitude="+gpsTracker.getLongitude());
                Log.e("TAG=","Longitude="+city);
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().setUserCreate(createUserCallback, mobileNumber, gender, firstName, lastName, category,newToken,"1",gpsTracker.getLatitude()+"",gpsTracker.getLongitude()+"",city);

            }


        }
    };

    @Override
    public void onResponse(Call<CategoryListMain> call, Response<CategoryListMain> response) {
        progressBar.setVisibility(View.GONE);
        if (response.isSuccessful()) {
            categoryListDataArrayList = new ArrayList<>();
            categoryListDataArrayList.addAll(response.body().getData());

            categoryCheckedArrayList = new ArrayList<>();
            for (int i = 0; i < categoryListDataArrayList.size(); i++) {
                CategoryChecked categoryChecked = new CategoryChecked();
                categoryChecked.setSetCheckedValue(false);
                categoryChecked.setCategoryId(categoryListDataArrayList.get(i).getId());
                categoryCheckedArrayList.add(categoryChecked);
            }

            recyclerView_category.setAdapter(categoryListAdapter);

        }
    }

    @Override
    public void onFailure(Call<CategoryListMain> call, Throwable t) {
        progressBar.setVisibility(View.GONE);
    }


    //Show all interested category data in recyclerview
    public class CategoryListAdapter extends
            RecyclerView.Adapter<CategoryListAdapter.MyViewHolder> {
        private int adapterPosition = -1;

        public CategoryListAdapter() {

        }

        @NonNull
        @Override
        public CategoryListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.item_interested_category, parent, false);
            return new CategoryListAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CategoryListAdapter.MyViewHolder holder, final int position) {
            holder.tv_category.setText(categoryListDataArrayList.get(position).getCategoryName());

            if (categoryCheckedArrayList.get(position).getSetCheckedValue()) {
                holder.switch_category.setChecked(true);
                Log.e("autoCalled=", "InsideTrue");

            } else {
                holder.switch_category.setChecked(false);
                Log.e("autoCalled1=", "InsideFalse");
            }


        }

        @Override
        public int getItemCount() {
            return categoryListDataArrayList.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView tv_category;
            private Switch switch_category;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_category = itemView.findViewById(R.id.tv_category);
                switch_category = itemView.findViewById(R.id.switch_category);
                switch_category.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        CategoryChecked categoryChecked = new CategoryChecked();
                        categoryChecked.setSetCheckedValue(isChecked);
                        categoryChecked.setCategoryName(categoryListDataArrayList.get(getAdapterPosition()).getCategoryName());
                        categoryCheckedArrayList.set(getAdapterPosition(), categoryChecked);
                        Log.e("size=", categoryCheckedArrayList.size() + "   " + getAdapterPosition());

                    }

                });

            }
        }
    }


    //callback for user created here and stored user data in sharedPreferences name USERCREATE
    //and also finishing all previous activities
    Callback<UserMain> createUserCallback = new Callback<UserMain>() {

        @Override
        public void onResponse(Call<UserMain> call, Response<UserMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {

                SharedPreferencesData sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
                sharedPreferencesData.createNewSharedPreferences(Constant.USERCREATE);
                sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.ID, response.body().getUserDetails().getId());
                sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.MOBILE_NUMBER,response.body().getUserDetails().getNumber());
                sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.GENDER, response.body().getUserDetails().getGender());
                sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.FIRST_NAME, response.body().getUserDetails().getFirstname());
                sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.LAST_NAME, response.body().getUserDetails().getLastname());
                sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.CATEGORY, response.body().getUserDetails().getCategory());
                sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.USERIMAGE, response.body().getUserDetails().getImage());
                sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.DATE_OF_BIRTH, response.body().getUserDetails().getDob());
                sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.ADDRESS, response.body().getUserDetails().getAddress());
                sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.EMAIL, response.body().getUserDetails().getEmail());
                sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.STATUS, response.body().getUserDetails().getStatus());
                sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.COMPLETE_STATUS, response.body().getUserDetails().getCompleteStatus());
                sharedPreferencesData.setSharedPreferenceData(Constant.USERCREATE, Constant.ADDEDAT, response.body().getUserDetails().getAddedAt());


                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        }

        @Override
        public void onFailure(Call<UserMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
    private String getAddressFromLatLong(double latitude,double longitude){
        String city="";
        if (latitude!=0.0&&longitude!=0.0){
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                city= addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


        return city;
    }
}
