package com.frcoder.notification.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;

import com.frcoder.notification.R;
import com.frcoder.notification.activity.LoginActivity;
import com.frcoder.notification.constans.Constant;
import com.frcoder.notification.constans.SharedPreferencesData;

public class MyDialogFragment extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                // set dialog icon
                //.setIcon(getResources().getDrawable(R.drawable.airtel_icon))
                // set Dialog Title
                .setTitle(getResources().getString(R.string.are_you_sure))
                // Set Dialog Message
                //.setMessage("This is a message")

                // positive button
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferencesData sharedPreferencesData = new SharedPreferencesData(getContext());
                        sharedPreferencesData.clearSharedPreferenceData(Constant.USERLOGIN);
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                })
                // negative button
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_SHORT).show();
                    }
                }).create();
    }
}