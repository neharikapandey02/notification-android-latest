package com.frcoder.notification.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.frcoder.notification.R;
import com.squareup.picasso.Picasso;


public class TestFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private TextView tv_test;

    private ImageView imgV_sliderImages;


    public TestFragment() {

    }


    public static TestFragment newInstance(String param1, String param2) {
        TestFragment fragment = new TestFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_test, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imgV_sliderImages = view.findViewById(R.id.imgV_sliderImages);
        tv_test = view.findViewById(R.id.tv_test);
        // tv_test.setText("THIS IS=" + mParam2);
       // Log.e("paramsCheck=", mParam1);

        Picasso.with(getContext())
                .load(mParam2)
                .placeholder(R.drawable.airtel_icon)
                .error(R.drawable.airtel_icon)
                .into(imgV_sliderImages);

        imgV_sliderImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mParam1!=null&&!mParam1.equalsIgnoreCase("")){
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mParam1));
                    startActivity(browserIntent);
                }else{
                    Toast.makeText(getContext(),getResources().getString(R.string.no_url_found),Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
