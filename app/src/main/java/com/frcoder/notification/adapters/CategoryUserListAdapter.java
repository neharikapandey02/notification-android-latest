package com.frcoder.notification.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.frcoder.notification.ModelsClasses.CategoryListUserMainData;
import com.frcoder.notification.R;
import com.frcoder.notification.activity.CategoryChecked;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CategoryUserListAdapter extends RecyclerView.Adapter<CategoryUserListAdapter.MyViewHolder> {
    private int adapterPosition = -1;
    private Context context;
    private List<CategoryListUserMainData> categoryListUserMainData;
    private ArrayList<CategoryChecked> categoryCheckedArrayList;

    public CategoryUserListAdapter(Context context,List<CategoryListUserMainData> categoryListUserMainData) {
        this.context=context;
        this.categoryListUserMainData=categoryListUserMainData;
        categoryCheckedArrayList=new ArrayList<>();
        for (int i=0;i<categoryListUserMainData.size();i++){

                CategoryChecked categoryChecked = new CategoryChecked();
                if (categoryListUserMainData.get(i).getUserSelected().equalsIgnoreCase("true")){
                    categoryChecked.setSetCheckedValue(true);
                }else{
                    categoryChecked.setSetCheckedValue(false);
                }

                categoryChecked.setCategoryId(categoryListUserMainData.get(i).getId());
                categoryCheckedArrayList.add(categoryChecked);

        }


    }

    @NonNull
    @Override
    public CategoryUserListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_interested_category, parent, false);
        return new CategoryUserListAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryUserListAdapter.MyViewHolder holder, final int position) {
        holder.tv_category.setText(categoryListUserMainData.get(position).getCategoryName());
        Picasso.with(context)
                .load(categoryListUserMainData.get(position).getBig())
                .placeholder(R.drawable.airtel_icon)
                .error(R.drawable.airtel_icon)
                .into(holder.imgV_notification);
        if (categoryCheckedArrayList.get(position).getSetCheckedValue()) {
            holder.switch_category.setChecked(true);
            Log.e("autoCalled=", "InsideTrue");

        } else {
            holder.switch_category.setChecked(false);
            Log.e("autoCalled1=", "InsideFalse");
        }


    }

    @Override
    public int getItemCount() {
        return categoryListUserMainData.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_category;
        private Switch switch_category;
        private ImageView imgV_notification;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_category = itemView.findViewById(R.id.tv_category);
            switch_category = itemView.findViewById(R.id.switch_category);
            imgV_notification = itemView.findViewById(R.id.imgV_notification);
            switch_category.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    CategoryChecked categoryChecked = new CategoryChecked();
                    categoryChecked.setSetCheckedValue(isChecked);
                    categoryChecked.setCategoryName(categoryListUserMainData.get(getAdapterPosition()).getCategoryName());
                    categoryCheckedArrayList.set(getAdapterPosition(), categoryChecked);
                    Log.e("size=", categoryListUserMainData.size() + "   " + getAdapterPosition());

                }

            });

        }
    }
}