package com.frcoder.notification.adapters;

import android.content.Context;
import android.content.Intent;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.frcoder.notification.ModelsClasses.CategoryListMainData;
import com.frcoder.notification.R;
import com.frcoder.notification.activity.CategoryListDetails;
import com.frcoder.notification.constans.Constant;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class CategoryListAdapterMainActivity extends RecyclerView.Adapter<CategoryListAdapterMainActivity.ViewHolder> {

    private Context context;
    private List<CategoryListMainData> listCategories;


    public CategoryListAdapterMainActivity(Context context, List<CategoryListMainData> categoryListData) {
        this.context = context;
        listCategories = categoryListData;
//        listCategories.addAll(categoryListData);


    }

    @NotNull
    @Override
    public CategoryListAdapterMainActivity.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_category_list, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(CategoryListAdapterMainActivity.ViewHolder holder, final int position) {
        holder.tv_categoryName.setText(listCategories.get(position).getCategoryName());
        holder.cardView.setOnClickListener(v -> {
            Intent intent = new Intent(context, CategoryListDetails.class);
            intent.putExtra(Constant.CATEGORYID,listCategories.get(position).getId());
            intent.putExtra(Constant.CATEGORY_NAME,listCategories.get(position).getCategoryName());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });
        Picasso.with(context)
                .load(listCategories.get(position).getLogo())
                .placeholder(R.drawable.music_test)
                .error(R.drawable.music_test)
                .into(holder.imgView_icon);


    }


    @Override
    public int getItemCount() {
        return listCategories.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView tv_categoryName;
        private ImageView imgView_icon;


        public ViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
            tv_categoryName = itemView.findViewById(R.id.tv_categoryName);
            imgView_icon = itemView.findViewById(R.id.imgView_icon);


        }
    }
}