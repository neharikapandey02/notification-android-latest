package com.frcoder.notification.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.frcoder.notification.ModelsClasses.MyFavouriteListMainData;
import com.frcoder.notification.R;
import com.frcoder.notification.activity.NotificationDetails;
import com.frcoder.notification.constans.Constant;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FavouriteListAdapter extends RecyclerView.Adapter<FavouriteListAdapter.ViewHolder> {
    List<MyFavouriteListMainData> myFavouriteListMainData;
    Context context;
    public FavouriteListAdapter(Context context, List<MyFavouriteListMainData> myFavouriteListMainData) {
        this.context=context;
        this.myFavouriteListMainData=myFavouriteListMainData;

    }

    @Override
    public FavouriteListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_favourite_list, parent, false);
        FavouriteListAdapter.ViewHolder viewHolder = new FavouriteListAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FavouriteListAdapter.ViewHolder holder, final int position) {
        Picasso.with(context)
                .load(myFavouriteListMainData.get(position).getImage())
                .placeholder(R.drawable.user)
                .error(R.drawable.user)
                .into(holder.circularimageNotification);
        holder.tv_title.setText(myFavouriteListMainData.get(position).getTitle());
        holder.cardviewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NotificationDetails.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constant.URL, myFavouriteListMainData.get(position).getImage());
                intent.putExtra(Constant.START_DATE, myFavouriteListMainData.get(position).getStartDate());
                intent.putExtra(Constant.END_DATE, myFavouriteListMainData.get(position).getEndDate());
                intent.putExtra(Constant.DETAILS, myFavouriteListMainData.get(position).getContent());
                intent.putExtra(Constant.NOTIFICATIONID, myFavouriteListMainData.get(position).getId());
                intent.putExtra(Constant.FAVOURITESTATUS, myFavouriteListMainData.get(position).getFavourite());
                intent.putExtra(Constant.TITLE, myFavouriteListMainData.get(position).getTitle());
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return myFavouriteListMainData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
       private CircularImageView circularimageNotification;
       private TextView tv_title;
       private CardView cardviewDetails;

        public ViewHolder(View itemView) {
            super(itemView);
            circularimageNotification=itemView.findViewById(R.id.circularimageNotification);
            tv_title=itemView.findViewById(R.id.tv_title);
            cardviewDetails=itemView.findViewById(R.id.cardviewDetails);


        }
    }
}