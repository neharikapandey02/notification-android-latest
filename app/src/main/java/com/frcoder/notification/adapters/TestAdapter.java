package com.frcoder.notification.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.frcoder.notification.R;


public class TestAdapter extends RecyclerView.Adapter<TestAdapter.MyAdapter>{

    public TestAdapter() {

    }

    @NonNull
    @Override
    public TestAdapter.MyAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater=LayoutInflater.from(viewGroup.getContext());
        View view=layoutInflater.inflate(R.layout.item_category_list,viewGroup,false);
        TestAdapter.MyAdapter testAdapter=new TestAdapter.MyAdapter(view);
        return testAdapter;
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter myAdapter, int i) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class MyAdapter extends RecyclerView.ViewHolder{

        public MyAdapter(@NonNull View itemView) {
            super(itemView);
        }
    }
}