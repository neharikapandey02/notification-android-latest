package com.frcoder.notification.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.frcoder.notification.ModelsClasses.CategoryWiseNotificationMainData;
import com.frcoder.notification.R;
import com.frcoder.notification.activity.TestActivity;
import com.frcoder.notification.constans.Constant;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CateogriesListDetailsAdapter extends RecyclerView.Adapter<CateogriesListDetailsAdapter.MyAdapter> {

    Context context;
    List<CategoryWiseNotificationMainData> categoryWiseNotificationMainData;

    public CateogriesListDetailsAdapter(Context context,List<CategoryWiseNotificationMainData> categoryWiseNotificationMainData) {
        this.context = context;
        this.categoryWiseNotificationMainData=categoryWiseNotificationMainData;
    }

    @NonNull
    @Override
    public CateogriesListDetailsAdapter.MyAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_categorylist_details, viewGroup, false);
        CateogriesListDetailsAdapter.MyAdapter categoryDetails = new CateogriesListDetailsAdapter.MyAdapter(view);
        return categoryDetails;
    }


    @Override
    public void onBindViewHolder(@NonNull CateogriesListDetailsAdapter.MyAdapter viewHolder, int position) {
        Picasso.with(context)
                .load(categoryWiseNotificationMainData.get(position).getImage())
                .placeholder(R.drawable.user)
                .error(R.drawable.user)
                .into(viewHolder.circularimageNotification);
        viewHolder.tv_title.setText(categoryWiseNotificationMainData.get(position).getTitle());
        viewHolder.relativeLayout_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TestActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constant.URL, categoryWiseNotificationMainData.get(position).getImage());
                intent.putExtra(Constant.START_DATE, categoryWiseNotificationMainData.get(position).getStartDate());
                intent.putExtra(Constant.END_DATE, categoryWiseNotificationMainData.get(position).getEndDate());
                intent.putExtra(Constant.DETAILS, categoryWiseNotificationMainData.get(position).getContent());
                intent.putExtra(Constant.NOTIFICATIONID, categoryWiseNotificationMainData.get(position).getId());
                intent.putExtra(Constant.FAVOURITESTATUS, categoryWiseNotificationMainData.get(position).getFavourite());
                intent.putExtra(Constant.TITLE, categoryWiseNotificationMainData.get(position).getTitle());
                intent.putExtra(Constant.MESSAGEID, categoryWiseNotificationMainData.get(position).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryWiseNotificationMainData.size();
    }

    public class MyAdapter extends RecyclerView.ViewHolder {
      CircularImageView circularimageNotification;
      TextView tv_title;
      CardView cardviewDetails;
      RelativeLayout relativeLayout_list;
        public MyAdapter(@NonNull View itemView) {
            super(itemView);
            circularimageNotification=itemView.findViewById(R.id.circularimageNotification);
            tv_title=itemView.findViewById(R.id.tv_title);
            cardviewDetails=itemView.findViewById(R.id.cardviewDetails);
            relativeLayout_list=itemView.findViewById(R.id.relativeLayout_list);
        }
    }
}
