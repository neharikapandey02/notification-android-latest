package com.frcoder.notification.ModelsClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryListUserMain {
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("user_category")
    @Expose
    private List<CategoryListUserMainData> userCategory = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<CategoryListUserMainData> getUserCategory() {
        return userCategory;
    }

    public void setUserCategory(List<CategoryListUserMainData> userCategory) {
        this.userCategory = userCategory;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
