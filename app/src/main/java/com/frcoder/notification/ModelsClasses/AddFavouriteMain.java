package com.frcoder.notification.ModelsClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddFavouriteMain {
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("notification_detail")
    @Expose
    private List<AddFavouriteMainData> notificationDetail = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<AddFavouriteMainData> getNotificationDetail() {
        return notificationDetail;
    }

    public void setNotificationDetail(List<AddFavouriteMainData> notificationDetail) {
        this.notificationDetail = notificationDetail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
