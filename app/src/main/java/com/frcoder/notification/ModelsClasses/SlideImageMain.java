package com.frcoder.notification.ModelsClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SlideImageMain {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("data")
    @Expose
    private List<SliderImageMainData> data = null;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<SliderImageMainData> getData() {
        return data;
    }

    public void setData(List<SliderImageMainData> data) {
        this.data = data;
    }
}
