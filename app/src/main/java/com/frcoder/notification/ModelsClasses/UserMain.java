package com.frcoder.notification.ModelsClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserMain {
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("user_details")
    @Expose
    private UserMainData userDetails;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public UserMainData getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserMainData userDetails) {
        this.userDetails = userDetails;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
