package com.frcoder.notification.ModelsClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyFavouriteListMain {
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("notification_list")
    @Expose
    private List<MyFavouriteListMainData> notificationList = null;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MyFavouriteListMainData> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<MyFavouriteListMainData> notificationList) {
        this.notificationList = notificationList;
    }
}
