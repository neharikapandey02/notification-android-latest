package com.frcoder.notification.ModelsClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UnreadCountMain {

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("notification_list")
    @Expose
    private List<UnreadCountMainData> notificationList = null;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UnreadCountMainData> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<UnreadCountMainData> notificationList) {
        this.notificationList = notificationList;
    }
}
