package com.frcoder.notification.ModelsClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryListMain {
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("data")
    @Expose
    private List<CategoryListMainData> data = null;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<CategoryListMainData> getData() {
        return data;
    }

    public void setData(List<CategoryListMainData> data) {
        this.data = data;
    }

}
