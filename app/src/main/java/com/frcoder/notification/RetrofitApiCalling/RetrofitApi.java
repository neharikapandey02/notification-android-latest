package com.frcoder.notification.RetrofitApiCalling;

import com.frcoder.notification.ModelsClasses.AddFavouriteMain;
import com.frcoder.notification.ModelsClasses.CategoryListMain;
import com.frcoder.notification.ModelsClasses.CategoryListUserMain;
import com.frcoder.notification.ModelsClasses.CategoryWiseNotificationMain;
import com.frcoder.notification.ModelsClasses.LastNotificatinMain;
import com.frcoder.notification.ModelsClasses.MakeReadMain;
import com.frcoder.notification.ModelsClasses.MyFavouriteListMain;
import com.frcoder.notification.ModelsClasses.OtpDataMain;
import com.frcoder.notification.ModelsClasses.SlideImageMain;
import com.frcoder.notification.ModelsClasses.SocialLinkMain;
import com.frcoder.notification.ModelsClasses.UnreadCountMain;
import com.frcoder.notification.ModelsClasses.UnreadNotificationMain;
import com.frcoder.notification.ModelsClasses.UserCreateMain;
import com.frcoder.notification.ModelsClasses.UserMain;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RetrofitApi {
    @FormUrlEncoded
    @POST("/webservice.php")
    Call<CategoryListMain> doCategoryList(@Field("category_list") String category_list,
                                          @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<SlideImageMain> doSlideImageList(@Field("banner_list") String otpVerification);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<UnreadNotificationMain> doUnreadNotification(@Field("notification_list") String notification_list,
                                                      @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<UnreadCountMain> doUnreadNotificationOnly(@Field("unread_notification_count") String notification_list,
                                                   @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<CategoryListUserMain> doCategoryUserList(@Field("user_category_list") String notification_list,
                                                  @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<CategoryListUserMain> doCategoryUserUpdate(@Field("update_user_category") String notification_list,
                                                    @Field("user_id") String user_id,
                                                    @Field("category") String category);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<AddFavouriteMain> doMakeFavourite(@Field("make_favourite") String make_favourite,
                                           @Field("user_id") String user_id,
                                           @Field("not_id") String not_id,
                                           @Field("favourite") String favourite);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<MakeReadMain> doMakeRead(@Field("make_read") String make_favourite,
                                  @Field("message_id") String user_id,
                                  @Field("user_id") String not_id);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<LastNotificatinMain> doLastNotification(@Field("last_unread") String last_unread,
                                                 @Field("id") String user_id);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<OtpDataMain> doOtpData(@Field("otpVerification") String otpVerification,
                                @Field("phone_number") String phone_number);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<UserMain> doUserCheck(@Field("check_user") String check_user,
                               @Field("number") String number,
                               @Field("device_token") String device_token,
                               @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<MyFavouriteListMain> doFavouriteList(@Field("get_all_favourite") String check_user,
                                              @Field("user_id") String number);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<SocialLinkMain> doSocialList(@Field("social_link") String check_user);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<CategoryWiseNotificationMain> doCategoryWiseList(@Field("notification_list") String check_user,
                                                          @Field("user_id") String user_id,
                                                          @Field("category") String category);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<UserMain> doUserCreate(@Field("user_register") String user_register,
                                @Field("number") String number,
                                @Field("gender") String gender,
                                @Field("firstname") String firstname,
                                @Field("lastname") String lastname,
                                @Field("category") String category,
                                @Field("dob") String dob,
                                @Field("email") String email,
                                @Field("address") String address,
                                @Field("complete_status") String complete_status,
                                @Field("device_id") String device_id,
                                @Field("device_type") String device_type,
                                @Field("latitude") String latitude,
                                @Field("longitude") String longitude,
                                @Field("city") String city);


    @Multipart
    @POST("/webservice.php")
    Call<UserCreateMain> doUserUpdate(@Part MultipartBody.Part image1,
                                      @Part("id") RequestBody id,
                                      @Part("update_profile") RequestBody update_profile,
                                      @Part("number") RequestBody number,
                                      @Part("gender") RequestBody gender,
                                      @Part("firstname") RequestBody firstname,
                                      @Part("lastname") RequestBody lastname,
                                      @Part("dob") RequestBody dob,
                                      @Part("address") RequestBody address,
                                      @Part("email") RequestBody email);

}
