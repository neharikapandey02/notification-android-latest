package com.frcoder.notification.RetrofitApiCalling;

import android.content.Context;


import com.frcoder.notification.ModelsClasses.AddFavouriteMain;
import com.frcoder.notification.ModelsClasses.CategoryListMain;
import com.frcoder.notification.ModelsClasses.CategoryListUserMain;
import com.frcoder.notification.ModelsClasses.CategoryWiseNotificationMain;
import com.frcoder.notification.ModelsClasses.LastNotificatinMain;
import com.frcoder.notification.ModelsClasses.MakeReadMain;
import com.frcoder.notification.ModelsClasses.MyFavouriteListMain;
import com.frcoder.notification.ModelsClasses.OtpDataMain;
import com.frcoder.notification.ModelsClasses.SlideImageMain;
import com.frcoder.notification.ModelsClasses.SocialLinkMain;
import com.frcoder.notification.ModelsClasses.UnreadCountMain;
import com.frcoder.notification.ModelsClasses.UnreadNotificationMain;
import com.frcoder.notification.ModelsClasses.UserCreateMain;
import com.frcoder.notification.ModelsClasses.UserMain;
import com.frcoder.notification.constans.Constant;
import com.frcoder.notification.constans.URLs;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHelper {

    private static RetrofitHelper ourInstance = new RetrofitHelper();

    public static RetrofitHelper getInstance() {
        return ourInstance;
    }

    private RetrofitHelper() {
    }

    RetrofitApi retrofitapi;

    public void init(final Context context) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).
                connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request.Builder ongoing = chain.request().newBuilder();
                        ongoing.addHeader("Content-Type", "application/json");
                        /*if(Prefs.getBoolean(context,"session",false)){
                            Log.d(">>>>>>>>>", "intercept: " + Prefs.getString(context,TOKENID,null));
                            ongoing.addHeader("Authorization","Bearer "+Prefs.getString(context,TOKENID, null));

                            return chain.proceed(ongoing.build());
                        }*/
                        return chain.proceed(ongoing.build());

                    }
                })
                .build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URLs.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        retrofitapi = retrofit.create(RetrofitApi.class);
    }


    public void getCategoryList(Callback<CategoryListMain> callback,String userId) {
        Call<CategoryListMain> response;


        response = retrofitapi.doCategoryList(Constant.CATEGORY_LIST,userId);
        response.enqueue(callback);
    }

    public void getSlideImageList(Callback<SlideImageMain> callback) {
        Call<SlideImageMain> response;


        response = retrofitapi.doSlideImageList(Constant.BANNER_LIST);
        response.enqueue(callback);
    }

    public void getUnreadNotification(Callback<UnreadNotificationMain> callback,String userId) {
        Call<UnreadNotificationMain> response;
        response = retrofitapi.doUnreadNotification(Constant.NOTIFICATION_LIST,userId);
        response.enqueue(callback);
    }

    public void getUnreadNotificationOnly(Callback<UnreadCountMain> callback, String userId) {
        Call<UnreadCountMain> response;
        response = retrofitapi.doUnreadNotificationOnly(Constant.NOTIFICATION_LIST,userId);
        response.enqueue(callback);
    }


    public void getUserCategoryList(Callback<CategoryListUserMain> callback,String userId) {
        Call<CategoryListUserMain> response;
        response = retrofitapi.doCategoryUserList(Constant.USER_CATEGORY_LIST,userId);
        response.enqueue(callback);
    }


    public void getUserCategoryUpdate(Callback<CategoryListUserMain> callback,String userId,String categoryId) {
        Call<CategoryListUserMain> response;
        response = retrofitapi.doCategoryUserUpdate(Constant.UPDATE_USER_CATEGORY,userId,categoryId);
        response.enqueue(callback);
    }

    public void setMakeFafouriteNotification(Callback<AddFavouriteMain> callback,String userId,String notificationId,String favouriteStatus) {
        Call<AddFavouriteMain> response;
        response = retrofitapi.doMakeFavourite(Constant.MAKE_FAVOURITE,userId,notificationId,favouriteStatus);
        response.enqueue(callback);
    }


    public void setMakeReadNotification(Callback<MakeReadMain> callback, String messageId, String userId) {
        Call<MakeReadMain> response;
        response = retrofitapi.doMakeRead(Constant.MAKE_READ,messageId,userId);
        response.enqueue(callback);
    }

    public void getLastNotificationDetails(Callback<LastNotificatinMain> callback, String userId) {
        Call<LastNotificatinMain> response;
        response = retrofitapi.doLastNotification(Constant.LAST_UNREAD,userId);
        response.enqueue(callback);
    }

    public void getOtpData(Callback<OtpDataMain> callback, String mobileNumber) {
        Call<OtpDataMain> response;

/*
        Call<OtpDataMain> response;
        OtpClass otpClass = new OtpClass();
        otpClass.setPhone_number(mobileNumber);
*/


        response = retrofitapi.doOtpData(Constant.OTP_VERIFICATION,mobileNumber);
        response.enqueue(callback);
    }

    public void setUserCreate(Callback<UserMain> callback, String mobileNumber, String gender, String firstName, String lastName, String category, String deviceId, String deviceType,String latitude,String longitude,String city) {
        Call<UserMain> response;
        response = retrofitapi.doUserCreate(Constant.USER_CHECK,mobileNumber,gender,firstName,lastName,category,"","","","1",deviceId,deviceType,latitude,longitude,city);
        response.enqueue(callback);
    }

    public void setUserCheck(Callback<UserMain> callback, String mobileNumber,String deviceToken) {
        Call<UserMain> response;
        /*UserCreateModel userCreateModel = new UserCreateModel();
        userCreateModel.setNumber(mobileNumber);
        userCreateModel.setGender(gender);
        userCreateModel.setFirstname(firstName);
        userCreateModel.setLastname(lastName);
        userCreateModel.setCategory(category);
        userCreateModel.setImage("");
        userCreateModel.setDob("");
        userCreateModel.setAddress("");
        userCreateModel.setEmail("");
        userCreateModel.setComplete_status("1");*/
        response = retrofitapi.doUserCheck(Constant.USER_CHECK,mobileNumber,deviceToken,Constant.DEVICETYPE);
        response.enqueue(callback);
    }

    public void setFavouriteList(Callback<MyFavouriteListMain> callback, String userId) {
        Call<MyFavouriteListMain> response;
        response = retrofitapi.doFavouriteList(Constant.GET_ALL_FAVOURITE,userId);
        response.enqueue(callback);
    }

    public void setSocialList(Callback<SocialLinkMain> callback) {
        Call<SocialLinkMain> response;
        response = retrofitapi.doSocialList(Constant.SOCIAL_LINKS);
        response.enqueue(callback);
    }


    public void setCategoryWiseList(Callback<CategoryWiseNotificationMain> callback, String userId, String categoryId) {
        Call<CategoryWiseNotificationMain> response;
        response = retrofitapi.doCategoryWiseList(Constant.NOTIFICATION_LIST,userId,categoryId);
        response.enqueue(callback);
    }


    public void setUpdateProfile(Callback<UserCreateMain> callback, String userId, String mobileNumber, String gender, String firstName, String lastName, String category, String dateOfBirth, String address, String email,String userImage) {
        Call<UserCreateMain> response;
        /*UserCreateModel userCreateModel = new UserCreateModel();
        userCreateModel.setNumber(mobileNumber);
        userCreateModel.setGender(gender);
        userCreateModel.setFirstname(firstName);
        userCreateModel.setLastname(lastName);
        userCreateModel.setCategory(category);
       userCreateModel.setImage("");
        userCreateModel.setDob(dateOfBirth);
        userCreateModel.setAddress(address);
        userCreateModel.setEmail(email);*/

        MultipartBody.Part image = null;


        if (userImage.equalsIgnoreCase("")) {
            image = MultipartBody.Part.createFormData("image", "");
        } else {
            File file = new File(userImage);

            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);

            //here we passed the key for sending the images userFiles[]
            image = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
        }


        RequestBody request_userId = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody update_profile = RequestBody.create(MediaType.parse("text/plain"), Constant.UPDATE_PROFILE);
        RequestBody request_number = RequestBody.create(MediaType.parse("text/plain"), mobileNumber);
        RequestBody request_gender = RequestBody.create(MediaType.parse("text/plain"), gender);
        RequestBody request_firstname = RequestBody.create(MediaType.parse("text/plain"), firstName);
        RequestBody request_lastname = RequestBody.create(MediaType.parse("text/plain"), lastName);
        //RequestBody request_category = RequestBody.create(MediaType.parse("text/plain"), category);
        RequestBody request_dob = RequestBody.create(MediaType.parse("text/plain"), dateOfBirth);
        RequestBody request_address = RequestBody.create(MediaType.parse("text/plain"), address);
        RequestBody request_email = RequestBody.create(MediaType.parse("text/plain"), email);




        response = retrofitapi.doUserUpdate(image,request_userId,update_profile,request_number,request_gender,request_firstname,request_lastname,request_dob,request_address,request_email);
        response.enqueue(callback);
    }

}
