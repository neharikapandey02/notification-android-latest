package com.frcoder.notification.constans;

import android.app.Application;
import android.content.Context;

import com.frcoder.notification.RetrofitApiCalling.RetrofitHelper;

public class NotificationBase extends Application {

    Context smart;

    @Override
    public void onCreate() {
        super.onCreate();
        smart = this.getApplicationContext();
        RetrofitHelper.getInstance().init(smart);
    }
}