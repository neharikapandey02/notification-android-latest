package com.frcoder.notification.constans;

public interface Constant {
    String USERLOGIN = "userlogin";
    String FLAG = "flag";
    String OTP = "otp";
    String MOBILE_NUMBER = "mobile_number";
    String GENDER = "gender";
    String FIRST_NAME = "first_name";
    String LAST_NAME = "last_name";
    String CATEGORY = "category";
    String USERCREATE = "userCreate";
    String ADDRESS = "address";
    String DATE_OF_BIRTH = "";
    String EMAIL = "email";
    String URL = "urls";
    String COMPLETE_STATUS ="complete_status" ;
    String USERIMAGE ="user_image";
    String OTP_VERIFICATION ="1" ;
    String CATEGORY_LIST ="1" ;
    String USER_REGISTRATION ="1" ;


    String USER_CHECK = "1";
    String ID ="id" ;
    String STATUS ="status" ;
    String ADDEDAT ="addedat" ;
    String BANNER_LIST ="1" ;
    String NOTIFICATION_LIST ="1" ;
    String START_DATE ="start_date" ;
    String DETAILS ="details" ;
    String END_DATE ="end_date" ;
    String UPDATE_PROFILE ="1" ;
    String MAKE_FAVOURITE ="1" ;
    String NOTIFICATIONID ="notificationid" ;
    String GET_ALL_FAVOURITE ="1" ;
    String FAVOURITESTATUS ="favouritestatus" ;
    String TITLE ="title" ;
    String USER_CATEGORY_LIST ="1" ;
    String UPDATE_USER_CATEGORY ="1" ;
    String MAKE_READ ="1" ;
    String MESSAGEID ="messageid" ;
    String CATEGORYID ="categoryId" ;
    String CATEGORY_NAME ="categoryname" ;
    String DEVICETYPE ="1" ;
    String SOCIAL_LINKS ="1" ;
    String NOTIFICATIONNEW ="new_notification" ;
    String LAST_UNREAD = "1";
}
